open Types
open Printer

type mbtype = {
    btype_desc: mbtype_desc;
    btype_loc: Lexing.position * Lexing.position
}

and mbtype_desc =
| Path of path
| Int
| Unit
| Empty
| Tuple of mbtype list
| Sum of mbtype list

let mkmbtype desc = { btype_desc = desc; btype_loc = Parsing.symbol_start_pos (), Parsing.symbol_end_pos () }

let mkMTyPath p = mkmbtype (Path p)
and mkMTyInt () = mkmbtype Int
and mkMTyUnit () = mkmbtype Unit
and mkMTyEmpty () = mkmbtype Empty
and mkMTyTuple ts = mkmbtype (Tuple ts)
and mkMTySum ts = mkmbtype (Sum ts)

type mtype = {
    mtype_desc: mtype_desc;
    mtype_loc: Lexing.position * Lexing.position
}

and mdecl = {
    name: label;
    var: mvar;
    ty: mtype;
    mdecl_loc: Lexing.position * Lexing.position
}

and mtype_desc =
| Abstract
| Concrete of mbtype
| Signature of mdecl list
| Functor of mvar * mtype * mtype
| Function of mbtype * mtype
| Monad of mbtype

let mkMDecl name var ty = { name = name; var = var; ty = ty; mdecl_loc = Parsing.symbol_start_pos (), Parsing.symbol_end_pos () }
let mkmtype desc = { mtype_desc = desc; mtype_loc = Parsing.symbol_start_pos (), Parsing.symbol_end_pos () }

let mkMTyAbstract () = mkmtype Abstract
and mkMTyConcrete t = mkmtype (Concrete t)
and mkMTySignature ss = mkmtype (Signature ss)
and mkMTyFunctor v t t' = mkmtype (Functor (v, t, t'))
and mkMTyFunction v t = mkmtype (Function (v, t))
and mkMTyMonad t = mkmtype (Monad t)

type mvalue = {
    mvalue_desc: mvalue_desc;
    mvalue_loc: Lexing.position * Lexing.position
}

and mvalue_desc =
| Var of vvar 
| Int of int
| Unit
| Tuple of mvalue list
| Sum of int * mvalue * mbtype list
| Op of mvalue * op * mvalue

let mkmval desc = { mvalue_desc = desc; mvalue_loc = Parsing.symbol_start_pos (), Parsing.symbol_end_pos () }

let mkMValVar v = mkmval (Var v)
and mkMValInt i = mkmval (Int i)
and mkMValUnit = mkmval (Unit)
and mkMValTuple vs = mkmval (Tuple vs)
and mkMValSum i v ts = mkmval (Sum (i, v, ts))
and mkMValOp v1 op v2 = mkmval (Op (v1, op, v2))

type mterm = {
    mterm_desc : mterm_desc;
    mutable mterm_used_mvars : mvar list;
    mterm_loc: Lexing.position * Lexing.position
}

and mdef = {
    name: label;
    var: mvar;
    term: mterm;
    mdef_loc: Lexing.position * Lexing.position
}

and mterm_desc =
| Path of path
| Type of mbtype
| Struct of mdef list
| Function of vvar * mbtype * mterm
| FunctionApp of mterm * mvalue
| Functor of mvar * mtype * mterm
| FunctorApp of mterm * path
| Seal of mterm * mtype
| Return of mvalue
| LetExpr of vvar * mterm * mterm
| LetOp of vvar * mvalue * mterm
| LetTuple of vvar list * mvalue * mterm
| Match of mvalue * (int * vvar * mterm) list

let mkMDef name var term = { name = name; var = var; term = term; mdef_loc = Parsing.symbol_start_pos (), Parsing.symbol_end_pos () }
let mkmterm (desc: mterm_desc) = { mterm_desc = desc; mterm_used_mvars = []; mterm_loc = Parsing.symbol_start_pos (), Parsing.symbol_end_pos () }

let mkMTmPath p = mkmterm (Path p)
let mkMTmStruct defs = mkmterm (Struct defs)
let mkMTmType t = mkmterm (Type t)
let mkMTmFunction v t e = mkmterm (Function (v, t, e))
let mkMTmFunctionApp e v = mkmterm(FunctionApp (e, v))
let mkMTmFunctor v t e = mkmterm (Functor (v, t, e))
let mkMTmFunctorApp e v = mkmterm (FunctorApp (e, v))
let mkMTmSeal e t = mkmterm (Seal (e, t))
let mkMTmReturn v = mkmterm (Return v)
let mkMTmLetExpr v e1 e2 = mkmterm (LetExpr (v, e1, e2))
let mkMTmLetOp x v e = mkmterm (LetOp (x, v, e))
let mkMTmLetTuple vs v e = mkmterm (LetTuple (vs, v, e))
let mkMTmMatch v cs = mkmterm (Match (v, cs))


let rec pp_mbtype t p =
    match t.btype_desc with
    | Path p' -> dumpl (string_of_path p') "mbtype.Path" p
    | Int -> dumpl "Int" "mbtype.Int" p
    | Unit -> dumpl "Unit" "mbtype.Unit" p
    | Empty -> dumpl "Empty" "mbtype.Empty" p
    | Tuple ts -> p |> enter "Tuple" |> dumpvs pp_mbtype ts |> leave
    | Sum ts -> p |> enter "Sum" |> dumpvs pp_mbtype ts |> leave

and pp_mdecl (d: mdecl) p =
    p |> enterk (Printf.sprintf "%s(%s)" (string_of_label d.name) (string_of_mvar d.var)) "mdecl" |> dumpv pp_mtype d.ty |> leave

and pp_mtype t p =
    match t.mtype_desc with
    | Abstract -> p |> enter "mtype.Abstract" |> leave
    | Concrete t' -> p |> enter "mtype.Conrete" |> dumpv pp_mbtype t' |> leave
    | Signature items -> p |> enter "mtype.Signature" |> dumpvs pp_mdecl items |> leave
    | Functor (x, t1, t2) -> p |> enter "mtype.Functor" |> dumpv pp_mtype t1 |> dumpv pp_mtype t2 |> leave
    | Function (t1, t2) -> p |> enter "mtype.Function" |> dumpv pp_mbtype t1 |> dumpv pp_mtype t2 |> leave
    | Monad t -> p |> enter "mtype.Monad" |> dumpv pp_mbtype t |> leave

and pp_mdef (d: mdef) p =
    p |> enterk (Printf.sprintf "%s(%s)" (string_of_label d.name) (string_of_mvar d.var)) "mdef" |> dumpv pp_mterm d.term |> leave

and pp_mvalue v p =
    match v.mvalue_desc with
    | Var x -> dumpl (string_of_vvar x) "mvalue.Var" p
    | Int x -> dumpl (string_of_int x) "mvalue.Int" p
    | Unit -> dumpl "()" "mvalue.Unit" p
    | Tuple vs -> p |> enter "mvalue.Tuple" |> dumpvs pp_mvalue vs |> leave
    | Sum (i, v, ts) -> p |> enterk (string_of_int i) "mvalue.Sum" |> dumpkv "value" pp_mvalue v |> dumpvs pp_mbtype ts |> leave
    | Op (v1, op, v2) -> p |> enterk (string_of_op op) "mvalue.Op" |> dumpv pp_mvalue v1 |> dumpv pp_mvalue v2 |> leave

and pp_mterm (t: mterm) (p: Printer.t) =
    match t.mterm_desc with
    | Path p' -> dumpl (string_of_path p') "mterm.Path" p
    | Type t -> p |> enter "mterm.Type" |> pp_mbtype t |> leave
    | Struct items -> p |> enter "mterm.Struct" |> dumpvs pp_mdef items |> leave
    | Function (x, t, e) -> p |> enter "mterm.Function" |> dumps (string_of_vvar x) |> dumpv pp_mbtype t |> dumpv pp_mterm e |> leave
    | FunctionApp (e1, e2) -> p |> enter "mterm.FunctionApp" |> dumpv pp_mterm e1 |> dumpv pp_mvalue e2 |> leave
    | Seal (e1, e2) -> p |> enter "mterm.Seal" |> dumpv pp_mterm e1 |> dumpv pp_mtype e2 |> leave
    | Return v -> p |> enter "mterm.Return" |> dumpv pp_mvalue v |> leave
    | LetExpr (x, e1, e2) -> p |> enter "mterm.LetExpr" |> dumps (string_of_vvar x) |> dumpv pp_mterm e1 |> dumpv pp_mterm e2 |> leave
    | LetOp (x, v, e) -> p |> enter "mterm.LetOp" |> dumps (string_of_vvar x) |> dumpv pp_mvalue v |> dumpv pp_mterm e |> leave
    | LetTuple (xs, v, e) -> p |> enter "mterm.LetTuple" |> dumps (String.concat "," (List.map string_of_vvar xs)) |> dumpv pp_mvalue v |> dumpv pp_mterm e |> leave
    | Functor (x, t, e) -> p |> enter "mterm.Functor" |> dumps (string_of_mvar x) |> dumpv pp_mtype t |> dumpv pp_mterm e |> leave
    | FunctorApp (e, v) -> p |> enter "mterm.FumctorApp" |> dumpv pp_mterm e |> dumps (string_of_path v) |> leave
    | Match (v, cs) -> p |> enter "mterm.Match" |> dumpv pp_mvalue v |> dumpvs (fun (i, v, e) p -> p |> dumps (string_of_int i) |> pp_mterm e) cs |> leave
