open Types




type bpath_root = Box | MVar of mvar
type bpath = bpath_root * (label list)

let bpath_root_eq r1 r2 =
    match r1, r2 with
    | MVar r1', MVar r2' -> mvar_eq r1' r2'
    | Box, Box -> true
    | _ -> false

let bpath_eq ((r1, ls1): bpath) ((r2, ls2): bpath) =
    bpath_root_eq r1 r2 && ls1 = ls2

let string_of_bpath ((r, ls): bpath) =
    (match r with Box -> "box" | MVar v -> string_of_mvar v) ^ "." ^ (String.concat "." (List.map string_of_label ls))

let bpath_to_path = function
    | (MVar v, ls) -> mkPath v ls
    | _ -> assert false

let mkBHole ls = (Box, ls)
let mkBVar v ls = (MVar v, ls)

let box_subs_opt (r1, ls1) (r2, ls2) (r3, ls3) : bpath option =
    let rec aux ls1 ls2 =
        match ls1, ls2 with
        | [], ls -> Some (r3, ls3 @ ls)
        | p::ps', l::ls' when p = l -> aux ps' ls'
        | _ -> None
    in if bpath_root_eq r1 r2 then aux ls1 ls2 else None

let box_subs p1 p2 p3 =
    match box_subs_opt p1 p2 p3 with
    | Some x -> x
    | None -> p2

let box_subs_multi patterns p =
    let patterns' = List.sort (fun ((_, ls1), _) ((_, ls2), _) -> (List.length ls2) - (List.length ls1)) patterns in
    let rec subs_name p = function
    | [] -> p
    | (x, y) :: patterns'' ->
        (match box_subs_opt x p y with Some x -> x | None -> subs_name p patterns'')
    in subs_name p patterns'





type stype = {
    stype_desc: stype_desc
}
and stype_desc =
| SUnit
| SInt
| SEmpty
| STuple of stype list
| SSum of stype list
| SRaw of extint
| SRef of (unit -> stype)
| SNone

let mkstype desc = { stype_desc = desc }
let mkSTyUnit () = mkstype SUnit
let mkSTyInt () = mkstype SInt
let mkSTyEmpty () = mkstype SEmpty
let mkSTyTuple ts = mkstype (STuple ts)
let mkSTySum ts = mkstype (SSum ts)
let mkSTyRaw n = mkstype (SRaw n)
let mkSTyNone () = mkstype SNone
let mkSTyRef f = mkstype (SRef f)

type svalue = {
    svalue_desc: svalue_desc;
    ty: stype
}

and svalue_desc =
| SVar of vvar 
| SInt of int
| SUnit
| STuple of svalue list
| SSum of int * svalue
| SOp of svalue * op * svalue

let typeof_svalue v = v.ty
let typesof_ssum v = 
    match v.ty.stype_desc with
    | SSum ts -> ts
    | _ -> assert false

let mkv desc ty = { svalue_desc = desc; ty = ty }
let mkSValVar v ty = mkv (SVar v) ty
let mkSValInt i = mkv (SInt i) (mkSTyInt ())
let mkSValUnit () = mkv SUnit (mkSTyUnit ())
let mkSValTuple vs = mkv (STuple vs) (mkSTyTuple (List.map typeof_svalue vs))
let mkSValSum i v tys = mkv (SSum (i, v)) (mkSTySum tys)
let mkSValOp v1 op v2 ty = mkv (SOp (v1, op, v2)) ty 

type sexpr = {
    sexpr_desc: sexpr_desc
}

and sexpr_desc =
| SCall of bpath * svalue list
| SReturn of svalue
| SLetExpr of vvar * sexpr * sexpr 
| SLetOp of vvar * svalue * sexpr 
| SLetTuple of vvar list * svalue * sexpr 
| SMatch of svalue * (vvar * sexpr) list 
| SUncoerc of vvar * stype * svalue * sexpr
| SCoerc of vvar * stype * svalue * sexpr

let mksexpr desc = { sexpr_desc = desc }
let mkSCall p args = mksexpr (SCall (p, args))
let mkSLetTuple vars x e = mksexpr (SLetTuple (vars, x, e))
let mkSLetExpr v e1 e2 = mksexpr (SLetExpr (v, e1, e2))
let mkSMatch v cases = mksexpr (SMatch (v, cases))
let mkSReturn v = mksexpr (SReturn v)
let mkSCoerc v1 t v2 e = mksexpr (SCoerc (v1, t, v2, e))
let mkSUncoerc v1 t v2 e = mksexpr (SUncoerc (v1, t, v2, e))
let mkSLetOp v vv e = mksexpr (SLetOp (v, vv, e))

let unpack_stuple t =
    match t.stype_desc with
    | STuple ts -> ts
    | _ -> assert false

let unpack_ssum t =
    match t.stype_desc with
    | SSum ts -> ts
    | _ -> assert false


type value = svalue

let rec subs_sexpr xys e =
    match e.sexpr_desc with
    | SCall (p, vs) -> mkSCall (box_subs_multi xys p) vs
    | SLetExpr (v, e1, e2) -> mkSLetExpr v (subs_sexpr xys e1) (subs_sexpr xys e2)
    | SLetOp (v, x, e) -> mkSLetOp v x (subs_sexpr xys e)
    | SLetTuple (vs, x, e) -> mkSLetTuple vs x (subs_sexpr xys e)
    | SMatch (x, cs) -> mkSMatch x (List.map (fun (v, e) -> v, subs_sexpr xys e) cs)
    | SUncoerc (v, t, x, e) -> mkSUncoerc v t x (subs_sexpr xys e)
    | SCoerc (v, t, x, e) -> mkSCoerc v t x (subs_sexpr xys e)
    | SReturn x -> mkSReturn x



module Interface = struct
    type arg = vvar * stype
    type decl = { name: bpath; args: arg list; ty: stype }
    type t = { provides: decl list; requires: decl list }

    let provides iface = iface.provides
    let requires iface = iface.requires
    let mkIface provides requires = { provides = provides; requires = requires }
    let mkDecl name args ty = { name = name; args = args; ty = ty }
    let concat ifaces = {
        provides = List.concat (List.map provides ifaces);
        requires = List.concat (List.map requires ifaces)
    }
    let subs_decl pattern substitute decl =
        mkDecl (box_subs pattern decl.name substitute) decl.args decl.ty
    let adjust_name f decl =
        mkDecl (f decl.name) decl.args decl.ty
    let subs pattern_prov substitute_prov pattern_req substitute_req iface =
        mkIface (List.map (subs_decl pattern_prov substitute_prov) (provides iface))
                (List.map (subs_decl pattern_req substitute_req) (requires iface))
    let map_decls f decls =
        List.map (fun decl -> f decl) decls
    let map_provides f iface =
        map_decls f iface.provides
    let map_requires f iface =
        map_decls f iface.requires
    let iter_decls f decls =
        List.iter (fun decl -> f decl) decls
    let iter_provides f iface =
        iter_decls f iface.provides
    let iter_requires f iface =
        iter_decls f iface.requires
    let provide_by_name name iface =
        List.find_opt (fun d -> name = d.name) iface.provides
    let require_by_name name iface =
        List.find_opt (fun d -> name = d.name) iface.requires
    let rename_args (vars: vvar list) (d: decl) =
        { d with args = List.map2 (fun v (_, ty) -> v, ty) vars d.args }
    let is_requires_empty iface =
        List.length iface.requires = 0
    let compare_name d1 d2 =
        compare d1.name d2.name
    let args d =
        d.args
end

let pp_iface (iface: Interface.t) =
    print_endline ("IFACE \n" ^ String.concat "\n" (List.map (fun (d: Interface.decl) -> Printf.sprintf "%s: %s" (string_of_bpath d.name) (String.concat "," (List.map (fun (v,_) -> string_of_vvar v) d.args))) iface.provides));
    print_endline ("IFACE \n" ^ String.concat "\n" (List.map (fun (d: Interface.decl) -> Printf.sprintf "%s: %s" (string_of_bpath d.name) (String.concat "," (List.map (fun (v,_) -> string_of_vvar v) d.args))) iface.requires))


module Impl = struct
    type arg = vvar * stype
    type def = { name: bpath; args: arg list; ret: stype; body: sexpr }
    let mkDef name args ret body = { name = name; args = args; ret = ret; body = body }
    type t = def list
    let concat ts = List.concat ts
    let subs (xys: (bpath * bpath) list) (impl: t) =
        let xys' = List.sort (fun ((_, ls1), _) ((_, ls2), _) -> (List.length ls2) - (List.length ls1)) xys in
        let rec subs_name i = function
        | [] -> i
        | (x, y) :: xys'' ->
            (match box_subs_opt x i.name y with
             | Some x -> { i with name = x }
             | None -> subs_name i xys'')
        in List.map (fun i -> {i with body = subs_sexpr xys i.body}) (List.map (fun i -> subs_name i xys') impl)

end
