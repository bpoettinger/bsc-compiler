
module type IDENT = sig
    type t = int
    type counter = unit -> t

    val make_counter: unit -> counter 
end

module Unique : IDENT = struct
    type t = int
    type counter = unit -> t

    let make_counter () =
        let c = ref 0 in
        fun () -> c := !c + 1; !c
end

module Const : IDENT = struct
    type t = int
    type counter = unit -> t

    let make_counter () =
        fun () -> 1
end
