open Types
open Context
open Ftypes







module Make (Ident: Ident.IDENT) = struct

    open Backend
    open Stypes
    open Interface



    let counter = Ident.make_counter ()

    let make_vvar () = VId (counter ())
    let make_mvar () = mkMId (counter ())
    let make_tvar () = TId (counter ())

    let define_decl (decl: Interface.decl) (body: sexpr) =
        Impl.mkDef decl.name decl.args decl.ty body

    let call_decl (decl: Interface.decl) =
        mkSCall decl.name (List.map (fun (x, ty) -> mkSValVar x ty) decl.args)

    let insert_args (n: int) (new_args: Interface.arg list) (decl: Interface.decl) =
        { decl with args = Myutil.list_insert_n n decl.args new_args }

    let match_decls (decls1: Interface.decl list) (decls2: Interface.decl list) =
        let decls1' = List.stable_sort (fun d1 d2 -> compare d1.name d2.name) decls1 in
        let decls2' = List.stable_sort (fun d1 d2 -> compare d1.name d2.name) decls2 in
        let rec aux d1 d2 =
            match d1, d2 with
            | [], _ -> []
            | _, [] -> []
            | x::d1', y::d2' when compare x.name y.name < 0 -> aux d1' d2
            | x::d1', y::d2' when compare x.name y.name > 0 -> aux d1 d2'
            | x::d1', y::d2' -> (x, y) :: (aux d1' d2')
        in aux decls1' decls2'

    let match_provides (iface1: Interface.t) (iface2: Interface.t) =
        match_decls iface1.provides iface2.provides

    let match_requires (iface1: Interface.t) (iface2: Interface.t) =
        match_decls iface1.requires iface2.requires
    
    let forward (iface: Interface.t) (x: bpath) (y: bpath) : Impl.t =
        let subs_x = Interface.subs_decl (mkBHole []) x in
        let subs_y = Interface.subs_decl (mkBHole []) y in
        Impl.concat [
            Interface.map_provides (fun d -> define_decl (subs_y d) (call_decl (subs_x d))) iface;
            Interface.map_requires (fun d -> define_decl (subs_x d) (call_decl (subs_y d))) iface
        ]

    let trap (x: bpath) (iface: Interface.t) : Impl.t =
        let subs_x d = Interface.subs_decl (mkBHole []) x d in
        Interface.map_requires (fun d -> define_decl (subs_x d) (call_decl (subs_x d))) iface

    let dig (iface: Interface.t) (num_vvars: int) (x: bpath) (y: bpath) (tys: stype list) =
        let var_x = make_vvar () in
        let ty_x = mkSTyTuple tys in
        let var_xs = List.map (fun _ -> make_vvar ()) tys in
        let arg_x = [(var_x, ty_x)] in
        let arg_xs = List.combine var_xs tys in
        let subs_x decl = insert_args num_vvars arg_x (Interface.subs_decl (mkBHole []) x decl) in
        let subs_xs decl = insert_args num_vvars arg_xs (Interface.subs_decl (mkBHole []) y decl) in
        let body_x decl = mkSLetExpr var_x (mkSReturn (mkSValTuple (List.map2 (fun v ty -> mkSValVar v ty) var_xs tys))) (call_decl (subs_x decl)) in
        let body_xs decl = mkSLetTuple var_xs (mkSValVar var_x ty_x) (call_decl (subs_xs decl)) in
        Impl.concat [
            Interface.map_provides (fun d -> define_decl (subs_xs d) (body_x d)) iface;
            Interface.map_requires (fun d -> define_decl (subs_x d) (body_xs d)) iface;
        ]

    let derelict (iface: Interface.t) (num_vvars: int) (x: bpath) (y: bpath) (value: svalue) =
        let var_v = make_vvar () in
        let arg_v = [(var_v, value.ty)] in
        let subs_y = Interface.subs_decl (mkBHole []) y in
        let subs_x decl = insert_args num_vvars arg_v (Interface.subs_decl (mkBHole []) x decl) in
        let body_v decl = mkSLetExpr var_v (mkSReturn value) (call_decl (subs_x decl)) in
        Impl.concat [
            Interface.map_provides (fun d -> define_decl (subs_y d) (body_v d)) iface;
            Interface.map_requires (fun d -> define_decl (subs_x d) (call_decl (subs_y d))) iface;
        ]

    let coerce (iface1: Interface.t) (iface2: Interface.t) (x: bpath) (y: bpath) =
        let sub_y = Interface.subs_decl (mkBHole []) y in
        let sub_x = Interface.subs_decl (mkBHole []) x in
        let generate_term1 (d1, d2) =
            let vars = List.map (fun _ -> make_vvar ()) d1.args in
            let vvar, vvar2 = make_vvar (), make_vvar () in
            let d1' = Interface.rename_args vars d1 in
            define_decl (sub_y d2)
                (List.fold_right2 (fun (v1, t1) (v2, t2) e ->
                    mkSCoerc v1 t1 (mkSValVar v2 t2) e)
                d1'.args d2.args (mkSLetExpr vvar (call_decl (sub_x d1')) (
                        mkSUncoerc vvar2 d2.ty (mkSValVar vvar d1.ty) (mkSReturn (mkSValVar vvar2 d2.ty))))) in
        let generate_term2 (d1, d2) =
            let vars = List.map (fun _ -> make_vvar ()) d1.args in
            let vvar, vvar2 = make_vvar (), make_vvar () in
            let d2' = Interface.rename_args vars d2 in
            define_decl (sub_x d1)
                (List.fold_right2 (fun (v1, t1) (v2, t2) e ->
                    mkSUncoerc v2 t2 (mkSValVar v1 t1) e)
                d1.args d2'.args (mkSLetExpr vvar (call_decl (sub_y d2')) (
                        mkSCoerc vvar2 d1.ty (mkSValVar vvar d2.ty) (mkSReturn (mkSValVar vvar2 d1.ty))))) in
        Impl.concat [
            List.map generate_term1 (match_provides iface1 iface2);
            List.map generate_term2 (match_requires iface1 iface2)
        ]

    let coerceInv (iface1: Interface.t) (iface2: Interface.t) (x: bpath) (y: bpath) =
        let sub_y = Interface.subs_decl (mkBHole []) y in
        let sub_x = Interface.subs_decl (mkBHole []) x in
        let generate_term1 (d1, d2) =
            let vars = List.map (fun _ -> make_vvar ()) d1.args in
            let vvar, vvar2 = make_vvar (), make_vvar () in
            let d1' = Interface.rename_args vars d1 in
            define_decl (sub_y d2)
                (List.fold_right2 (fun (v1, t1) (v2, t2) e ->
                    mkSUncoerc v1 t1 (mkSValVar v2 t2) e)
                d1'.args d2.args (mkSLetExpr vvar (call_decl (sub_x d1')) (
                        mkSCoerc vvar2 d2.ty (mkSValVar vvar d1.ty) (mkSReturn (mkSValVar vvar2 d2.ty))))) in
        let generate_term2 (d1, d2) =
            let vars = List.map (fun _ -> make_vvar ()) d1.args in
            let vvar, vvar2 = make_vvar (), make_vvar () in
            let d2' = Interface.rename_args vars d2 in
            define_decl (sub_x d1)
                (List.fold_right2 (fun (v1, t1) (v2, t2) e ->
                    mkSCoerc v2 t2 (mkSValVar v1 t1) e)
                d1.args d2'.args (mkSLetExpr vvar (call_decl (sub_y d2')) (
                        mkSUncoerc vvar2 d1.ty (mkSValVar vvar d2.ty) (mkSReturn (mkSValVar vvar2 d1.ty))))) in
        Impl.concat [
            List.map generate_term1 (match_provides iface1 iface2);
            List.map generate_term2 (match_requires iface1 iface2)
        ]

    let contract (iface: Interface.t) (num_vvars: int) (x: bpath) (xs: bpath list) (tys: stype list) =
        let var_v = make_vvar () in
        let ty_v = mkSTySum tys in
        let var_vs = List.map (fun _ -> make_vvar ()) xs in
        let arg_v = [(var_v, mkSTySum tys)] in
        let arg_vs x ty = [(x, ty)] in
        let sub_x decl = insert_args num_vvars arg_v (Interface.subs_decl (mkBHole []) x decl) in
        let sub_xs v ty x decl = insert_args num_vvars (arg_vs v ty) (Interface.subs_decl (mkBHole []) x decl) in
        let body_x i v decl ty = mkSLetExpr var_v (mkSReturn (mkSValSum i (mkSValVar v ty) tys)) (call_decl (sub_x decl)) in
        let body_xs_case decl v (x, ty) = v, call_decl (insert_args num_vvars (arg_vs v ty) (Interface.subs_decl (mkBHole []) x decl)) in
        let body_xs decl = mkSMatch (mkSValVar var_v ty_v) (List.map2 (body_xs_case decl) var_vs (List.combine xs tys)) in
        Impl.concat ([
            Interface.map_requires (fun d -> define_decl (sub_x d) (body_xs d)) iface;
        ] @ (List.map2 (
            fun (i, x') (v', ty') -> 
                Interface.map_provides (fun d -> define_decl (sub_xs v' ty' x' d) (body_x i v' d ty')) iface)
            (Myutil.count xs)
            (List.combine var_vs tys)))





    let rec elaborate_context' ctx =
        match ctx with
        | [] -> [], Interface.mkIface [] []
        | (TVar v)::ctx' ->
            elaborate_context' ctx'
        | (MVarNl (v, _, t, _)) :: ctx'
        | (MVar (v, t, _))::ctx' ->
            let types1, iface1 = elaborate_context' ctx' in
            let iface2 = elaborate_ftype t ctx' in
            types1, Interface.concat [iface1; iface2]
        | (VVar (v, t))::ctx' -> 
            let types1, iface1 = elaborate_context' ctx' in
            let type2 = elaborate_fbtype t in
            (v, type2) :: types1, iface1
    
    and elaborate_context ctx =
        let types, iface = elaborate_context' ctx in
        List.rev types, iface

    and elaborate_fbtype (ty: Ftypes.fbtype) : stype =
        match !((find ty).fbtype_desc) with
        | Unit -> mkSTyUnit ()
        | Int -> mkSTyInt ()
        | Raw k -> mkSTyRaw !k
        | Var v -> mkSTyRef (fun () -> let t = elaborate_fbtype ty in match !((find ty).fbtype_desc) with Var _ -> mkSTyRaw Infty | _ -> t)
        | Tuple ts -> mkSTyTuple (List.map elaborate_fbtype ts)
        | Sum ts -> mkSTySum (List.map elaborate_fbtype ts)
        | Link t -> elaborate_fbtype (Ftypes.find t)
        | _ -> assert false

    and elaborate_ftype ty ctx =
        match ty.ftype_desc with
        | Monad t ->
            let tys1, _ = elaborate_context ctx in
            let ty2 = elaborate_fbtype t in
            Interface.mkIface [Interface.mkDecl (mkBHole ["main"]) tys1 ty2] []
        | Extended (d, t') ->
            let new_var_x = make_vvar () in
            let ctx' = ctx |> extend_vvar new_var_x d in
            elaborate_ftype t' ctx'
        | Exists (vars, t')
        | Forall (vars, t') ->
            let ctx' = ctx |> extend_tvars vars in
            elaborate_ftype t' ctx'
        | Arrow (d, t') ->
            let ctx' = ctx |> extend_vvar (make_vvar ()) d in
            let iface1 = elaborate_ftype t' ctx' in
            let iface2 = elaborate_ftype t' ctx' in
            Interface.mkIface (Interface.provides iface1) (Interface.requires iface2)
        | Lolli (t'', t') ->
            let iface1 = elaborate_ftype t'' ctx in
            let iface2 = elaborate_ftype t' ctx in
            let iface1' = Interface.subs (mkBHole []) (mkBHole ["arg"]) (mkBHole []) (mkBHole ["arg"]) iface1 in
            let iface2' = Interface.subs (mkBHole []) (mkBHole ["res"]) (mkBHole []) (mkBHole ["res"]) iface2 in
            Interface.concat [Interface.mkIface (Interface.requires iface1') (Interface.provides iface1');
                              Interface.mkIface (Interface.provides iface2') (Interface.requires iface2')]
        | Record items ->
                let ifaces = List.map (fun (l, t) -> Interface.subs (mkBHole []) (mkBHole [l]) (mkBHole []) (mkBHole [l]) (elaborate_ftype t ctx)) items in
            Interface.concat ifaces
        | Dummy _ ->
            Interface.mkIface [] []




    type value = { value: svalue; value_ty: fbtype }
    let typeof_value v = v.value_ty

    let mkv value ty = { value = value; value_ty = ty }
    let mkValVar v ty = mkv (mkSValVar v (elaborate_fbtype ty)) ty
    let mkValInt i = mkv (mkSValInt i) (mkTyInt ())
    let mkValUnit () = mkv (mkSValUnit ()) (mkTyUnit ())
    let mkValTuple vs = mkv (mkSValTuple (List.map (fun v -> v.value) vs)) (mkTyTuple (List.map (fun v -> v.value_ty) vs))
    let mkValSum i v tys = mkv (mkSValSum i v.value (List.map elaborate_fbtype tys)) (mkTySum tys)

    type t = { impl: Impl.t; ty: ftype; ctx: context }
    let typeof t = t.ty

    let mkt (desc: string) (impl: Impl.t) ty ctx =
        (*let context = String.concat "," (List.map (fun e -> match e with VVar (v, _) -> string_of_vvar v | MVar (v, t, _) | MVarNl (v, _, t, _) -> (match t.ftype_desc with Extended (tt, _) -> "!" | _ -> "") ^ string_of_mvar v | _ -> "" ) (List.rev ctx)) in
        print_endline ("=== " ^ desc ^ " " ^ context); 
        print_endline (String.concat "\n" (List.map (fun (d: Impl.def) -> Printf.sprintf "%s: %s" (string_of_bpath d.name) (String.concat "," (List.map (fun (v, t) -> Printf.sprintf "%s" (string_of_vvar v)) d.args))) impl));
        *){ impl = impl; ty = ty; ctx = ctx }

    let mkVar v ty ctx =
        let ctx' = ctx |> replace_mvar_with_mvars v [] [] in
        let iface = elaborate_ftype ty ctx' in
        mkt ("mkVar " ^ string_of_mvar v) (forward iface (mkBVar v []) (mkBHole [])) ty ctx
    
    let mkFabs v t e =
        mkt "mkFabs" (Impl.subs [(mkBHole [], mkBHole ["res"]); (mkBVar v [], mkBHole ["arg"])] e.impl)
    
    let mkFapp e1 e2 =
        let iface = elaborate_ftype (lolli_result_type e1.ty) e1.ctx in
        mkt "mkFapp" (Impl.concat [
            Impl.subs [(mkBHole [], mkBHole ["arg"])] e2.impl;
            e1.impl;
            forward iface (mkBHole ["res"]) (mkBHole [])
        ])
    
    let mkBabs v t e ty ctx =
        let new_mvar = make_mvar () in
        let iface = elaborate_ftype e.ty e.ctx in
        let n = Context.count_vvars ctx in
        let t' = elaborate_fbtype t in
        mkt "mkBabs" (Impl.concat [
            Impl.subs [(mkBHole [], mkBVar new_mvar [])] e.impl;
            map_provides (fun d -> define_decl d (call_decl (subs_decl (mkBHole []) (mkBVar new_mvar []) d))) iface;
            map_requires (fun d -> define_decl (insert_args n [(v, t')] d) (call_decl (subs_decl (mkBHole []) (mkBVar new_mvar []) d))) iface
        ]) ty ctx
    
    let mkBapp e1 e2 ty ctx =
        let new_mvar = make_mvar () in
        let new_vvar = make_vvar () in
        let n = Context.count_vvars ctx in
        let iface = elaborate_ftype (arrow_result_type e1.ty) e1.ctx in
        mkt "mkBapp" (Impl.concat [
            Impl.subs [(mkBHole [], mkBVar new_mvar [])] e1.impl;
            map_provides (fun d -> define_decl d (mkSLetExpr new_vvar (mkSReturn e2.value) (call_decl (insert_args n [(new_vvar, e2.value.ty)] (subs_decl (mkBHole []) (mkBVar new_mvar []) d))))) iface;
            map_requires (fun d -> define_decl (subs_decl (mkBHole []) (mkBVar new_mvar []) d) (call_decl d)) iface
        ]) ty ctx
    
    let mkRecord items =
        mkt "mkRecord" (Impl.concat (List.map (fun (l, e) -> Impl.subs [(mkBHole [], mkBHole [l])] e.impl) items))
    
    let mkLetRecord items e1 e2 =
        let ss = List.map (fun (l, x) -> mkBHole [l], mkBVar x []) items in
        mkt "mkLetRecord" (Impl.concat [
            Impl.subs ss e1.impl;
            e2.impl
        ])
    
    let mkTabs vs e =
        let _ = elaborate_ftype e.ty e.ctx in
        mkt "mkTabs" e.impl
    
    let mkTapp e ts =
        let new_mvar = make_mvar () in
        let xs, ty' = unpack_forall e.ty in
        let iface1 = elaborate_ftype (subs_ftype (List.combine xs ts) ty') e.ctx in
        let ctx' = Context.extend_tvars xs e.ctx in
        let iface2 = elaborate_ftype ty' ctx' in
        mkt "mkTapp" (Impl.concat [
            Impl.subs [(mkBHole [], mkBVar new_mvar [])] e.impl;
            coerce iface2 iface1 (mkBVar new_mvar []) (mkBHole [])
        ])
    
    let mkPack ts e ty ctx =
        let new_mvar = make_mvar () in
        let xs, ty' = unpack_exists ty in
        let iface1 = elaborate_ftype (subs_ftype (List.combine xs ts) ty') e.ctx in
        let ctx' = Context.extend_tvars xs e.ctx in
        let iface2 = elaborate_ftype ty' ctx' in
        mkt "mkPack" (Impl.concat [
            Impl.subs [(mkBHole [], mkBVar new_mvar [])] e.impl;
            coerceInv iface1 iface2 (mkBVar new_mvar []) (mkBHole [])
        ]) ty ctx
    
    let mkLetPack ts x e1 e2 =
        mkt "mkLetPack" (Impl.concat [
            Impl.subs [(mkBHole [], mkBVar x [])] e1.impl;
            e2.impl
        ])
    
    let mkStar t =
        mkt "mkStar" []
    
    let mkReturn (v: value) ty ctx =
        let _, _ = elaborate_context ctx in
        let vvars = Context.vvars ctx in
        let vvars' = List.map (fun (v, ty) -> v, elaborate_fbtype ty) vvars in
        mkt "mkReturn" (Impl.concat [
            [Impl.mkDef (mkBHole ["main"]) vvars' v.value.ty (mkSReturn v.value)];
        ]) ty ctx
    
    let mkLetExpr v e1 e2 ty ctx =
        let new_vvar = make_vvar () in
        let x1, x2 = make_mvar (), make_mvar () in
        let ty1' = elaborate_fbtype (unpack_monad e1.ty) in
        let iface1 = elaborate_ftype e1.ty ctx in
        let iface2 = elaborate_ftype e2.ty ctx in
        let n = Context.count_vvars e2.ctx in
        let dmain = (match Interface.provide_by_name (mkBHole ["main"]) iface1 with Some d -> d | _ -> assert false) in
        let sub d = subs_decl (mkBHole []) (mkBVar x2 []) (insert_args n [(new_vvar, ty1')] d) in
        mkt "mkLetExpr" (Impl.concat [
            Impl.subs [(mkBHole [], mkBVar x1 [])] e1.impl;
            Impl.subs [(mkBHole [], mkBVar x2 [])] e2.impl;
            Interface.map_provides (fun d -> define_decl d (mkSLetExpr new_vvar (call_decl (subs_decl (mkBHole []) (mkBVar x1 []) dmain)) (call_decl (sub d)))) iface2;
            Interface.map_requires (fun d -> define_decl (sub d) (call_decl d)) iface2
        ]) ty ctx
    
    let mkLetTuple vs (v: value) e ty ctx =
        let x = make_mvar () in
        let iface = elaborate_ftype e.ty ctx in
        let n = Context.count_vvars ctx in
        let vs' = List.combine vs (unpack_stuple v.value.ty) in
        mkt "mkLetTuple" (Impl.concat [
            Impl.subs [(mkBHole [], mkBVar x [])] e.impl;
            map_provides (fun d -> define_decl d (mkSLetTuple vs v.value (call_decl (insert_args n vs' (Interface.subs_decl (mkBHole []) (mkBVar x []) d))))) iface;
        ]) ty ctx
    
    let mkMatch (v: value) cs ty ctx =
        let new_vvar = make_vvar () in
        let xs = List.map (fun _ -> make_mvar ()) cs in
        let iface = elaborate_ftype ty ctx in
        let n = Context.count_vvars ctx in
        let cs' decl = List.map2 (fun (v', e) x -> v', call_decl (insert_args n [(v', v.value.ty)] (subs_decl (mkBHole []) (mkBVar x []) decl))) cs xs in
        let req' x = map_requires (fun d -> define_decl (insert_args n [(new_vvar, v.value.ty)] (subs_decl (mkBHole []) (mkBVar x []) d)) (call_decl d)) iface in
        mkt "mkMatch" (Impl.concat ( [
            map_provides (fun d -> define_decl d (mkSMatch v.value (cs' d))) iface
        ] @ (List.map2 (fun (v, e) x -> Impl.subs [(mkBHole [], mkBVar x [])] e.impl) cs xs))
          @ (List.flatten (List.map req' xs))) ty ctx
        
    let mkLetOp x v1 op v2 e ty ctx =
        let new_mvar = make_mvar () in
        let iface = elaborate_ftype e.ty ctx in
        let n = Context.count_vvars ctx in
        mkt "mkLetOp" (Impl.concat [
            Impl.subs [(mkBHole [], mkBVar new_mvar [])] e.impl;
            map_provides (fun d -> define_decl d (mkSLetOp x v1.value op v2.value (call_decl (insert_args n [(x, mkSTyInt ())] (Interface.subs_decl (mkBHole []) (mkBVar new_mvar []) d))))) iface;
        ]) ty ctx
    
    let mkCloseR ty e =
        mkt "mkCloseR" e.impl

    let mkCloseL e =
        mkt "mkCloseL" e.impl
    
    let mkContract x xs e tys ty ctx =
        let _, ty' = unpack_extended (mtypeof x ctx) in
        let ctx' = ctx |> Context.until_mvar x in
        let iface = elaborate_ftype ty' ctx' in
        let n = Context.count_vvars ctx in
        let tys' = List.map elaborate_fbtype tys in
        mkt "mkContract" (Impl.concat [
            e.impl;
            contract iface n (mkBVar x []) (List.map (fun x -> mkBVar x []) xs) tys'
        ]) ty ctx
    
    let mkDerelict old_mvar e orig_ty ctx = 
        let new_mvar = make_mvar () in
        let ty = mtypeof old_mvar ctx in
        let _, ty' = unpack_extended ty in
        let ctx' = ctx |> Context.until_mvar old_mvar in
        let iface = elaborate_ftype ty' ctx' in
        let n = Context.count_vvars ctx' in
        mkt "mkDerelict" (Impl.concat [
            Impl.subs [(mkBVar old_mvar [], mkBVar new_mvar [])] e.impl;
            derelict iface n (mkBVar old_mvar []) (mkBVar new_mvar []) (mkSValUnit ())
        ]) orig_ty ctx
    
    let mkDig x e tys ty ctx =
        let new_mvar = make_mvar () in
        let ty' = mtypeof x ctx in
        let ty'' = extended_inner ty' in
        let ctx' = ctx |> Context.until_mvar x in
        let iface = elaborate_ftype ty'' ctx' in
        let n = Context.count_vvars ctx' in
        let tys' = List.map elaborate_fbtype tys in
        mkt ("mkDig" ^ string_of_mvar x) (Impl.concat [
            Impl.subs [(mkBVar x [], mkBVar new_mvar [])] e.impl;
            dig iface n (mkBVar x []) (mkBVar new_mvar []) tys'
        ]) ty ctx
    
    let mkWeak x e ty ctx =
        let ty' = mtypeof x ctx in
        let iface = elaborate_ftype ty' ctx in
        mkt "mkWeak" (Impl.concat [
            e.impl;
            trap (mkBVar x []) iface
        ]) ty ctx
    
    let mkCloseRInv e =
        mkt "mkCloseRInv" e.impl
    
    let mkSubtype x e ty ctx =
        let new_mvar = make_mvar () in
        let ctx' = ctx |> until_mvar x in
        let iface1 = elaborate_ftype (mtypeof x ctx) ctx' in
        let iface2 = elaborate_ftype (mtypeof x e.ctx) ctx' in
        mkt "mkSubtype" (Impl.concat [
            Impl.subs [(mkBVar x [], mkBVar new_mvar [])] e.impl;
            coerce iface1 iface2 (mkBVar x []) (mkBVar new_mvar [])
        ]) ty ctx
    
    let mkDuplicate x e old_ty ctx =
        let new_mvar = make_mvar () in
        let new_vvar = make_vvar () in
        let ctx' = e.ctx |> until_mvar x in
        let ty = mtypeof x e.ctx in
        let ext', ty' = unpack_extended ty in
        let ext'' = elaborate_fbtype ext' in
        let iface = elaborate_ftype ty' ctx' in
        assert (is_requires_empty iface);
        let n = count_vvars ctx' in
        mkt ("mkDuplicate " ^ string_of_mvar x) (Impl.concat [
            Impl.subs [(mkBVar x [], mkBVar new_mvar [])] e.impl;
            Interface.map_provides (fun d -> define_decl (subs_decl (mkBHole []) (mkBVar new_mvar []) (insert_args n [(new_vvar, ext'')] d)) (call_decl (subs_decl (mkBHole []) (mkBVar x []) d))) iface
        ]) old_ty ctx
    
    let pp_fterm t p = assert false

end
