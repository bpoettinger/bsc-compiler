%{
    open Types
    open Mtypes
%}

%token UNIT
%token INT
%token LPAREN
%token RPAREN
%token EQUAL
%token PLUS
%token MINUS
%token TIMES
%token SLASH
%token PERCENT
%token COMMA
%token DOT
%token COLON
%token SEMICOLON
%token SEAL
%token ARROW1
%token ARROW2
%token LET
%token IN
%token CASE
%token OF
%token FN
%token AS
%token FUNCTOR
%token SIG
%token STRUCT
%token END
%token TYPE
%token INL
%token INR
%token <int> CI
%token RETURN
%token <string> IDENTIFIER_UPPER
%token <string> IDENTIFIER_LOWER
%token <int> NUMBER
%token EOF
%token MONAD
%token LABRACK
%token RABRACK

%nonassoc SUM_TERM
%left PLUS MINUS
%left TIMES SLASH PERCENT
%nonassoc MODULE_TERM
%nonassoc IDENTIFIER NUMBER UNIT LPAREN INL INR SEAL

%start module_type
%type <Mtypes.mtype> module_type

%start base_type
%type <Mtypes.mbtype> base_type

%start program
%type <Mtypes.mterm> program

%%

module_type: TYPE { mkMTyAbstract () }
           | TYPE EQUAL base_type { mkMTyConcrete $3 }
           | signature { $1 }
           | FUNCTOR LPAREN IDENTIFIER_UPPER COLON module_type RPAREN ARROW1 module_type { mkMTyFunctor (mkMName $3) $5 $8 }
           | base_type ARROW1 module_type { mkMTyFunction $1 $3 }
           | MONAD LABRACK base_type RABRACK { mkMTyMonad $3 }
           ;;

base_type: base_type_single { $1 }
         | base_type_product { mkMTyTuple $1 }
         | base_type_sum { mkMTySum $1 }
         ;;

base_type_single: path { mkMTyPath $1 }
                | INT { mkMTyInt () }
                | UNIT { mkMTyUnit () }
                | LPAREN base_type RPAREN { $2 }
                ;;

base_type_product: base_type_single TIMES base_type_single { [$1; $3] }
                 | base_type_product TIMES base_type_single { $1 @ [$3] }
                 ;;

base_type_sum: base_type_single PLUS base_type_single { [$1; $3] }
             | base_type_sum PLUS base_type_single { $1 @ [$3] }
             ;;

path: IDENTIFIER_UPPER { mkPath (mkMName $1) [] }
    | path DOT IDENTIFIER_LOWER { match unpack_path $1 with v, ls -> mkPath v (ls@[$3]) }
    ;;

signature: SIG END { mkMTySignature [] }
         | SIG declarations END { mkMTySignature $2 }
         ;;

declarations: declaration { [$1] }
            | declarations COMMA declaration { $1 @ [$3] }
            ;;

declaration: IDENTIFIER_LOWER LPAREN IDENTIFIER_UPPER RPAREN COLON module_type { mkMDecl $1 (mkMName $3) $6 }
           ;;

arg_list: LPAREN IDENTIFIER_LOWER COLON base_type RPAREN { mkMTmFunction (VName $2) $4 }
        | LPAREN IDENTIFIER_LOWER COLON base_type RPAREN arg_list { fun body -> mkMTmFunction (VName $2) $4 ($6 body) }

arg_list_functor: LPAREN IDENTIFIER_UPPER COLON module_type RPAREN { mkMTmFunctor (mkMName $2) $4 }
                | LPAREN IDENTIFIER_UPPER COLON module_type RPAREN arg_list_functor { fun body -> mkMTmFunctor (mkMName $2) $4 ($6 body) }

module_term: path { mkMTmPath $1 }
           | TYPE base_type { mkMTmType $2 }
           | STRUCT definitions END { mkMTmStruct $2 }
           | STRUCT END { mkMTmStruct [] }
           | FUNCTOR arg_list_functor ARROW1 module_term %prec MODULE_TERM { $2 $4 }
           | FN arg_list ARROW1 module_term %prec MODULE_TERM { $2 $4 }
           | path function_apps { $2 (mkMTmPath $1) } 
           | LPAREN module_term RPAREN function_apps { $4 $2 } 
           | path functor_apps { $2 (mkMTmPath $1) } 
           | LPAREN module_term RPAREN functor_apps { $4 $2 } 
           | module_term SEAL module_type %prec MODULE_TERM { mkMTmSeal $1 $3 }
           | RETURN core_value { mkMTmReturn $2 }
           | LET IDENTIFIER_LOWER EQUAL module_term IN module_term %prec MODULE_TERM { mkMTmLetExpr (VName $2) $4 $6 }
           | LET IDENTIFIER_LOWER EQUAL core_value IN module_term %prec MODULE_TERM { mkMTmLetOp (VName $2) $4 $6 }
           | LET LPAREN components RPAREN EQUAL core_value IN module_term %prec MODULE_TERM { mkMTmLetTuple $3 $6 $8 }
           | CASE core_value OF INL LPAREN IDENTIFIER_LOWER RPAREN ARROW2 module_term SEMICOLON INR LPAREN IDENTIFIER_LOWER RPAREN ARROW2 module_term %prec MODULE_TERM { mkMTmMatch $2 [(0, VName $6, $9); (1, VName $13, $16)] }
           | CASE core_value OF cases { mkMTmMatch $2 $4 }
           ;;

components: IDENTIFIER_LOWER COMMA IDENTIFIER_LOWER { [VName $1; VName $3] }
          | components COMMA IDENTIFIER_LOWER { $1 @ [VName $3] }

case: CI LPAREN IDENTIFIER_LOWER RPAREN ARROW2 module_term { ($1-1, VName $3, $6) }

cases: case { [$1] }
     | case SEMICOLON cases { $1 :: $3 }
     ;;

program: module_term { mkMTmSeal $1 (mkMTySignature [mkMDecl "main" (mkMName "MAIN") (mkMTyFunction (mkMTyInt ()) (mkMTyMonad (mkMTyInt ())))]) }
       ;;

function_apps: core_value { fun f -> mkMTmFunctionApp f $1 }
             | function_apps core_value { fun f -> mkMTmFunctionApp ($1 f) $2 }
             ;;

functor_apps: LPAREN path RPAREN { fun f -> mkMTmFunctorApp f $2 }
            | functor_apps LPAREN path RPAREN { fun f -> mkMTmFunctorApp ($1 f) $3 }
            ;;

core_value: IDENTIFIER_LOWER { mkMValVar (VName $1) }
          | NUMBER { mkMValInt $1 }
          | LPAREN RPAREN { mkMValUnit }
          | LPAREN tuple_elements RPAREN { mkMValTuple $2 }
          | INL LPAREN core_value RPAREN AS base_type %prec SUM_TERM { match $6.btype_desc with Sum [t; t'] -> mkMValSum 0 $3 [t; t'] | _ -> raise Parsing.Parse_error }
          | INR LPAREN core_value RPAREN AS base_type %prec SUM_TERM { match $6.btype_desc with Sum [t; t'] -> mkMValSum 1 $3 [t; t'] | _ -> raise Parsing.Parse_error }
          | CI LPAREN core_value RPAREN AS base_type %prec SUM_TERM { match $6.btype_desc with Sum tys when $1 < 10 && List.length tys = $1 -> mkMValSum ($1-1) $3 tys | _ -> raise Parsing.Parse_error }
          | LPAREN core_value RPAREN { $2 }
          | core_value PLUS core_value { mkMValOp $1 Add $3 }
          | core_value MINUS core_value { mkMValOp $1 Sub $3 }
          | core_value TIMES core_value { mkMValOp $1 Mul $3 }
          | core_value SLASH core_value { mkMValOp $1 Div $3 }
          | core_value PERCENT core_value { mkMValOp $1 Mod $3 }
          ;;

tuple_elements: core_value COMMA core_value { [$1; $3] }
              | tuple_elements COMMA core_value { $1 @ [$3] }

definitions: definition { [$1] }
           | definition COMMA definitions { $1 :: $3 }

definition: IDENTIFIER_LOWER LPAREN IDENTIFIER_UPPER RPAREN EQUAL module_term { mkMDef $1 (mkMName $3) $6 }
         ;;
