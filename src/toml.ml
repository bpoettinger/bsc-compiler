open Types
open Printf
open Stypes

let bpath_to_ml ((r, ls): bpath) =
    (match r with Box -> "box" | MVar v -> string_of_mvar v) ^ "_" ^ (String.concat "_" (List.map string_of_label ls))

let mvar_to_ml (v: mvar) =
    match v.mvar_desc with
    | MId x -> sprintf "m%i" x
    | MName x -> x ^ "'"
    | _ -> assert false

let vvar_to_ml (v: vvar) =
    match v with
    | VId x -> sprintf "v%i" x
    | VName x -> x ^ "'"

let rec resolve_ref (t: stype) =
    match t.stype_desc with
    | SRef f -> resolve_ref (f ())
    | _ -> t

let rec type_to_ml (t: stype) =
    let rec aux c t =
        match t.stype_desc with
        | SInt -> "int"
        | SUnit -> "unit"
        | STuple ts ->
            if c
            then "(" ^ (String.concat "*" (List.map (aux true) ts)) ^ ")"
            else String.concat "*" (List.map (aux true) ts)
        | SSum ts ->
            if c
            then "((" ^ String.concat ", " (List.map (aux true) ts) ^ (sprintf ") sum%d)" (List.length ts))
            else "(" ^ String.concat ", " (List.map (aux true) ts) ^ (sprintf ") sum%d" (List.length ts))
        | SRaw k -> sprintf "raw_%s" (string_of_extint k)
        | SRef f -> aux c (f ())
        | _ -> assert false
    in aux false t

let op_to_ml op =
    match op with
    | Add -> "+"
    | Sub -> "-"
    | Mul -> "*"
    | Div -> "/"
    | Mod -> "mod"

let value_to_ml (v: svalue) =
    let rec aux c v =
        match v.svalue_desc with
        | SVar x -> vvar_to_ml x
        | SInt x -> string_of_int x
        | SUnit -> "()"
        | STuple vs ->
            "(" ^ (String.concat ", " (List.map (aux true) vs)) ^ ")"
        | SSum (i, v') ->
            let tys = unpack_ssum (typeof_svalue v) in
            let n = List.length tys in
            if c
            then sprintf "(Sum%d_%d %s)" n (i+1) (aux true v')
            else sprintf "Sum%d_%d %s" n (i+1) (aux true v')
        | SOp (v1, op, v2) ->
            if c
            then sprintf "(%s %s %s)" (aux true v1) (op_to_ml op) (aux true v2)
            else sprintf "%s %s %s" (aux true v1) (op_to_ml op) (aux true v2)
    in aux false v

let arg_to_ml ((v, ty): Interface.arg) : string =
    sprintf "(%s: %s)" (string_of_vvar v) (type_to_ml ty)

let args_to_ml (args: Interface.arg list) : string =
    String.concat " " (List.map arg_to_ml args)

let params_to_ml params =
    String.concat " " (List.map value_to_ml params)

let encode =
    let counter = Ident.Unique.make_counter () in
    let rec make_writer (t: stype) : string * string =
        let id = "x" ^ string_of_int (counter ()) in
        id, Printf.sprintf "encode %s" id
    and bla (t1: stype) (t2: stype) : string * string =
        let id = "x" ^ string_of_int (counter ()) in
        match t1.stype_desc, t2.stype_desc with
        | SRef f, _ -> bla (f ()) t2
        | _, SRef f -> bla t1 (f ())
        | SUnit, SUnit -> id, id
        | SInt, SInt -> id, id
        | STuple ts1, STuple ts2 ->
            let xs = List.map2 bla ts1 ts2 in
            let patterns, writers = List.split xs in
            Printf.sprintf "(%s)" (String.concat ", " patterns), Printf.sprintf "(%s)" (String.concat ", " writers)
        | SSum ts1, SSum ts2 ->
            let n = List.length ts1 in
            assert (List.length ts1 = List.length ts2);
            id, Printf.sprintf "(match %s with %s)" id
                (String.concat " | " (List.mapi (fun i (p, w) -> Printf.sprintf "Sum%d_%d %s -> Sum%d_%d (%s)" n (i+1) p n (i+1) w) (List.map2 bla ts1 ts2)))
        | SRaw k1, SRaw k2 -> id, id
        | _, SRaw k -> make_writer t1
        | _ -> failwith (type_to_ml t1 ^ ";" ^ type_to_ml t2)
    in bla


let decode =
    let counter = Ident.Unique.make_counter () in
    let rec bla (t1: stype) (t2: stype) : string * string =
        let id = "x" ^ string_of_int (counter ()) in
        match t1.stype_desc, t2.stype_desc with
        | SUnit, SUnit -> id, id
        | SInt, SInt -> id, id
        | STuple ts1, STuple ts2 ->
            let xs = List.map2 bla ts1 ts2 in
            let patterns, writers = List.split xs in
            Printf.sprintf "(%s)" (String.concat ", " patterns), Printf.sprintf "(%s)" (String.concat ", " writers)
        | SSum ts1, SSum ts2 ->
            let n = List.length ts1 in
            assert (List.length ts1 = List.length ts2);
            id, Printf.sprintf "(match %s with %s)" id
                (String.concat " | " (List.mapi (fun i (p, w) -> Printf.sprintf "Sum%d_%d %s -> Sum%d_%d (%s)" n (i+1) p n (i+1) w) (List.map2 bla ts1 ts2)))
        | SRaw k1, SRaw k2 -> id, id
        | _, SRaw k -> make_reader t1
        | _ -> assert false
    and make_reader (t: stype) : string * string =
        let id = "x" ^ string_of_int (counter ()) in
        id, Printf.sprintf "decode %s" id
    in bla

let rec expr_to_ml (e: sexpr) =
    match e.sexpr_desc with
    | SReturn v -> value_to_ml v
    | SCall (p, params) -> sprintf "%s %s" (bpath_to_ml p) (params_to_ml params) 
    | SLetExpr (v, e1, e2) -> sprintf "let %s = %s in %s" (vvar_to_ml v) (expr_to_ml e1) (expr_to_ml e2)
    | SLetTuple (vs, v, e) -> sprintf "let (%s) = %s in %s" (String.concat ", " (List.map vvar_to_ml vs)) (value_to_ml v) (expr_to_ml e)
    | SLetOp (v, x, e) -> sprintf "let %s = %s in %s" (vvar_to_ml v) (value_to_ml x) (expr_to_ml e)
    | SMatch (v, cs) ->
        let n = List.length cs in
        sprintf "match %s with %s" (value_to_ml v) (String.concat " | " (List.mapi (fun i (v, e) -> sprintf "Sum%d_%d %s -> %s" n (i+1) (vvar_to_ml v) (expr_to_ml e)) cs))
    | SCoerc (v1, t, v2, e) ->
        let pattern, conv = encode (resolve_ref (typeof_svalue v2)) (resolve_ref t) in
        sprintf "let %s : %s = (match %s with %s -> %s) in %s" (vvar_to_ml v1) (type_to_ml t) (value_to_ml v2) pattern conv (expr_to_ml e)
    | SUncoerc (v1, t, v2, e) ->
        let pattern, conv = decode (resolve_ref t) (resolve_ref (typeof_svalue v2)) in
        sprintf "let %s : %s = (match %s with %s -> %s) in %s" (vvar_to_ml v1) (type_to_ml t) (value_to_ml v2) pattern conv (expr_to_ml e)

let def_to_ml (def: Impl.def) : string =
    sprintf "%s %s : %s = %s" (bpath_to_ml def.name) (args_to_ml def.args) (type_to_ml def.ret) (expr_to_ml def.body)

let bootstrap =
    let gen_sum n =
        let aux_tvars n =
            String.concat ", " (List.map (fun k -> sprintf "'t%d" k) (Myutil.range 1 n)) in
        let aux_constr n =
            String.concat " | " (List.map (fun k -> sprintf "Sum%d_%d of 't%d" n k k) (Myutil.range 1 n)) in
        sprintf "type (%s) sum%d = %s" (aux_tvars n) n (aux_constr n)
    in "let encode x = Marshal.to_string x []\nand decode x = Marshal.from_string x 0\n;;\ntype raw_infty = string\n;;\n" ^ String.concat "\n" (List.map gen_sum (Myutil.range 1 49)) ^ "\n;;\n"

let main = "print_endline (string_of_int (box_main_main \"\" \"\" 1 \"\"))"

let defs_to_ml_raw defs =
    String.concat "\n    and " (List.map def_to_ml defs)

let defs_to_ml (defs: Impl.def list) : string =
    bootstrap ^ "let rec " ^ String.concat "\n    and " (List.map def_to_ml defs) ^ "\n;;\n" ^ main
