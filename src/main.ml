open Printf

module MyBackend = Backendflex.Make (Ident.Unique)
module MyFrontend = Frontend.Make (MyBackend)
open MyFrontend

let black = "\027[30m";;
let red = "\027[31m";;
let green = "\027[32m";;
let yellow = "\027[33m";;
let blue = "\027[34m";;
let magenta = "\027[35m";;
let cyan = "\027[36m";;
let white = "\027[37m";;
let reset = "\027[0m";;

let () = Printexc.record_backtrace true
let () =
    if Array.length Sys.argv <> 2 then begin
        printf "Expecting one argument: path\n";
        exit 1
    end else begin
        let path = Array.get Sys.argv 1 in
        let lexbuf = Lexing.from_channel (open_in path) in
        try
            let mterm = Parser.program Lexer.main lexbuf in
            let () = MyFrontend.linearize_mterm mterm in
            print_endline "(*";
            let printer = Printer.make stdout "| " in
            printf "%sParsed file '%s'%s\n\n" green path reset;
            printf "%sParsed mterm:%s\n" green reset;
            let _ = Mtypes.pp_mterm mterm printer in
            print_endline "*)";

            let ctx = Context.empty_context in
            let fterm = elaborate_mterm mterm ctx in
            (*printf "\n%sElaborated ftype:%s\n" green reset;
            let _ = Ftypes.pp_ftype (MyBackend.typeof fterm) printer in
            printf "\n%sElaborated fterm:%s\n" green reset;
            let _ = Backendflex.Make.pp_fterm fterm printer in
            let _ = Middleend.pp_constraints printer in
            print_newline ();
            Middleend.solve_constraints ();
            let _ = Middleend.pp_constraints printer in
            print_endline "";*)
            print_endline "(*=====";
            ignore (MyBackend.pp_fterm fterm (Printer.make stdout "| "));
            print_endline "=====*)";
            MyBackend.elaborate_fterm fterm [];
            print_endline (Toml.defs_to_ml !MyBackend.fragments)

        with
        | Lexer.InvalidChar (char, _, pos) ->
            printf "%sLexer: Invalid character '%c' (%s:%d:%d) %s\n" red char path (pos.pos_lnum+1) (pos.pos_cnum - pos.pos_bol) reset
        | Parsing.Parse_error ->
            let pos = lexbuf.lex_curr_p in
            printf "%sParser: Syntax error (%s:%d:%d) %s\n" red path (pos.pos_lnum+1) (pos.pos_cnum - pos.pos_bol) reset
        | Errors.CompiliationFailed -> exit (-1)
        | _ -> Printexc.print_backtrace stdout;
    end
