open Types
open Printer
open Ftypes
open Context


module type BACKEND = sig

    type value
    type t

    val make_mvar : unit -> mvar
    val make_vvar : unit -> vvar
    val make_tvar : unit -> tvar

    val typeof : t -> ftype
    val typeof_value : value -> fbtype

    val mkValVar : vvar -> fbtype -> value
    val mkValInt : int -> value
    val mkValUnit : unit -> value
    val mkValTuple : value list -> value
    val mkValSum : int -> value -> fbtype list -> value
    val mkValOp : value -> op -> value -> fbtype -> value

    val mkVar : mvar -> ftype -> context -> t
    val mkStar : fbtype -> ftype -> context -> t
    val mkFabs : mvar -> ftype -> t -> ftype -> context -> t
    val mkFapp : t -> t -> ftype -> context -> t
    val mkBabs : vvar -> fbtype -> t -> ftype -> context -> t
    val mkBapp : t -> value -> ftype -> context -> t
    val mkTabs : tvar list -> t -> ftype -> context -> t
    val mkTapp : t -> fbtype list -> ftype -> context -> t
    val mkReturn : value -> ftype -> context -> t
    val mkLetExpr : vvar -> t -> t -> ftype -> context -> t
    val mkLetTuple : vvar list -> value -> t -> ftype -> context -> t
    val mkLetOp : vvar -> value -> t -> ftype -> context -> t
    val mkMatch : value -> (int * vvar * t) list -> ftype -> context -> t
    val mkLetPack : tvar list -> mvar -> t -> t -> ftype -> context -> t
    val mkRecord : (label * t) list -> ftype -> context -> t
    val mkPack : fbtype list -> t -> ftype -> context -> t
    val mkLetRecord : (label * mvar) list -> t -> t -> ftype -> context -> t
    val mkCloseR : fbtype -> t -> ftype -> context -> t
    val mkCloseL : t -> ftype -> context -> t
    val mkContract : mvar -> mvar list -> t -> fbtype list -> ftype -> context -> t
    val mkDerelict : mvar -> t -> ftype -> context -> t
    val mkDig : mvar -> t -> fbtype list -> ftype -> context -> t
    val mkWeak : mvar -> t -> ftype -> context -> t
    val mkCloseRInv : t -> ftype -> context -> t
    val mkSubtype : mvar -> t -> ftype -> context -> t
    val mkDuplicate : mvar -> t -> ftype -> context -> t

    val pp_fterm : t -> Printer.t -> Printer.t

end
