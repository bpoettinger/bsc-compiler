open Printf

type t = {
    out: out_channel;
    indention_pattern: string;
    mutable cur_indention: int
}

let make (out: out_channel) (indention_pattern: string) : t = {
    out = out;
    indention_pattern = indention_pattern;
    cur_indention = 0
}

let append_new_line t =
    fprintf t.out "\n";
    for i = 1 to t.cur_indention do
        fprintf t.out "%s" t.indention_pattern
    done

let dumpkv (k: ('a, unit, string) format) pp v t =
    fprintf t.out "[%s] " (sprintf k);
    pp v t;
    t

let dumpv pp v t =
    pp v t;
    t

let dumpvs pp vs t =
    List.fold_left (fun t v -> dumpv pp v t) t vs

let dumps s t =
    fprintf t.out "%s" s;
    append_new_line t;
    t

let enter ty t =
    fprintf t.out "%s" ty;
    t.cur_indention <- t.cur_indention + 1;
    append_new_line t;
    t

let enterk k ty t =
    fprintf t.out "%s :: %s" k ty;
    t.cur_indention <- t.cur_indention + 1;
    append_new_line t;
    t

let leave t =
    t.cur_indention <- t.cur_indention - 1;
    append_new_line t;
    t

let dumpl k ty t =
    fprintf t.out "%s :: %s" k ty;
    append_new_line t;
    t
