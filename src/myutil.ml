
let list_split_n n xs =
    let rec aux k xs1 xs2 =
        if k == 0 then List.rev xs1, xs2 else
        match xs2 with
        | [] -> List.rev xs1, xs2
        | x::xs2' -> aux (k-1) (x::xs1) xs2'
    in aux n [] xs

let list_insert_n n xs news =
    let xs1, xs2 = list_split_n n xs in
    xs1 @ news @ xs2

let count xs =
    List.mapi (fun i x -> i, x) xs

let rec range m n =
    if m <= n then m :: (range (m+1) n) else []
