open Types
open Printer
open Ftypes

module Make (Backend: Backend.BACKEND) = struct

    open Backend
    include Context

    type t = Backend.t
    let typeof = Backend.typeof
    let typeof_value = Backend.typeof_value
    type value = Backend.value
    let mkValInt = Backend.mkValInt
    let mkValUnit = Backend.mkValUnit
    let mkValTuple = Backend.mkValTuple
    let mkValSum = Backend.mkValSum
    let mkValVar = Backend.mkValVar
    let mkValOp = Backend.mkValOp

    let make_tvar = Backend.make_tvar
    let make_mvar = Backend.make_mvar
    let make_vvar = Backend.make_vvar

    type fterm = context -> Backend.t

    let unique_type (es: (int * vvar * t) list): ftype = 
        let aux t (_, _, e) =
            match t with
            | None -> Some (typeof e) 
            | Some t' -> unify_extended t' (typeof e); Some t'
        in match List.fold_left aux None es with
        | None -> assert false
        | Some t -> t

    let constraints = ref []
    let add_constraint (t1: fbtype) (t2: fbtype) : unit =
        constraints := (t1, t2) :: !constraints
    let pp_constraint ((lower, upper): fbtype * fbtype) (p: Printer.t) =
        p |> enter "constraint" |> enter "lower" |> pp_fbtype lower |> leave |> enter "upper" |> pp_fbtype upper |> leave |> leave
    let pp_constraints (p: Printer.t) =
        p |> enter "constraints" |> dumpvs pp_constraint !constraints |> leave
    let solve_constraints () =
        let partition = List.map (fun (_, upper) -> upper, ref []) !constraints in
        List.iter (fun (lower, upper) -> let _, c = List.find (fun (v, _) -> eq_fbtype v upper) partition in c := lower :: !c) !constraints;
        List.iter (fun (v, c) -> unify_var_only (mkTySum !c) v) partition;
        let rec undefined_to_unit t =
            match !((find t).fbtype_desc) with
            | Var v -> unify_var_only t (mkTyUnit ())
            | Sum ts -> List.iter undefined_to_unit ts
            | Tuple ts -> List.iter undefined_to_unit ts
            | _ -> ()
        in List.iter (fun (lower, _) -> undefined_to_unit lower) !constraints



    let rec mkTmVar v ctx =
        print_endline ("(*" ^ ("mkTmVar " ^ string_of_mvar v) ^ "*)");
        let ttt = mtypeof v ctx in
        if is_last_entry v ctx
        then begin mark_mvar v ctx; mkVar v (ttt) ctx end
        else assert false
    and mkTmVarInner v ctx =
        print_endline ("(*" ^ ("mkTmVarInner " ^ string_of_mvar v) ^ "*)");
        let ty = mtypeof v ctx in
        let d', ty' = unpack_extended ty in
        let tys = vvars_after_mvar v ctx in
        if (List.length tys) > 0 then
            let d'' = mkTyTuple tys in
            add_constraint d'' d';
            let rec aux = function
                | [] -> assert false
                | MVarNl (v', _, t, _) :: ctx'
                | MVar (v', t, _) :: ctx' when mvar_eq v v' -> mkTmSubtype v d'' (mkTmDig v (mkTmVar v))
                | MVarNl (v', _, t, _) :: ctx'
                | MVar (v', t, _) :: ctx' -> mkTmWeak v' (aux ctx')
                | VVar (v', t) :: ctx' -> mkTmCloseRInv (aux ctx')
                | _ :: ctx' -> aux ctx'
            in (aux ctx) ctx
        else
            let rec aux = function
                | [] -> assert false
                | MVarNl (v', _, t, _) :: ctx'
                | MVar (v', t, _) :: ctx' when mvar_eq v v' -> mkTmDerelict v (mkTmVar v)
                | MVarNl (v', _, t, _) :: ctx'
                | MVar (v', t, _) :: ctx' -> mkTmWeak v' (aux ctx')
                | _ :: ctx' -> aux ctx'
            in (aux ctx) ctx
    and mkTmVarInnerBanged v ctx =
        print_endline ("(*" ^ ("mkTmVarInnerBanged " ^ string_of_mvar v) ^ "*)");
        let d1', d2' = mkTyVar (make_tvar ()), mkTyVar (make_tvar ()) in
        let d' = mkTyTuple [d1'; d2'] in
        ctx |> mkTmSubtype v d' (mkTmDig v (mkTmVarInner v))
    and mkTmVarOuter v ctx =
        print_endline ("(*" ^ ("mkTmVarOuter " ^ string_of_mvar v) ^ "*)");
        let ty = mtypeof v ctx in
        let tys = vvars_after_mvar v ctx in
        assert (List.length tys = 0);
        mark_mvar v ctx;
        mkVar v ty ctx
    and mkTmFabs ty e ctx =
        let v = make_mvar () in
        print_endline ("(*" ^ ("mkTmFabs " ^ string_of_mvar v) ^ "*)");
        let ctx' = ctx |> extend_mvar v ty in
        let e' = e v ctx' in
        let e'' = if is_mvar_marked v ctx' then e' else mkWeak v e' (typeof e') ctx' in
        mkFabs v ty e'' (mkTyLolli ty (typeof e'')) ctx
    and mkTmFabsNl x ty e ctx =
        let v = make_mvar () in
        print_endline ("(*" ^ ("mkTmFabsNl " ^ string_of_mvar v ^ "(" ^ string_of_mvar x ^ ")") ^ "*)");
        let ctx' = ctx |> extend_mvarnl v x ty in
        let e' = e v ctx' in
        let e'' = if is_mvar_marked v ctx' then e' else mkWeak v e' (typeof e') ctx' in
        mkFabs v ty e'' (mkTyLolli ty (typeof e'')) ctx
    and mkTmFapp e1 e2 ctx =
        print_endline ("(*" ^ "mkTmFapp" ^ "*)");
        let e1' = e1 ctx in
        let e2' = e2 ctx in
        mkFapp e1' e2' (lolli_result_type (typeof e1')) ctx
    and mkTmTabs vs e ctx =
        print_endline ("(*" ^ "mkTmTabs" ^ "*)");
        let e' = e ctx in
        mkTabs vs e' (mkTyForall vs (typeof e')) ctx
    and mkTmTapp e ts ctx =
        print_endline ("(*" ^ "mkTmTapp" ^ "*)");
        let e' = e ctx in
        let vars, ty' = unpack_forall (typeof e') in
        let ts' = List.combine vars ts in
        let ty'' = subs_ftype ts' ty' in
        mkTapp e' ts ty'' ctx
    and mkTmBabs x ty e ctx =
        print_endline ("(*" ^ "mkTmBabs" ^ "*)");
        let ctx' = ctx |> extend_vvar x ty in
        let e' = e ctx' in
        mkBabs x ty e' (mkTyArrow ty (typeof e')) ctx
    and mkTmBapp e v ctx =
        print_endline ("(*" ^ "mkTmBapp" ^ "*)");
        let e' = e ctx in
        let argty, resty = unpack_arrow (typeof e') in
        if eq_fbtype (find argty) (find (typeof_value v))
        then mkBapp e' v resty ctx
        else Errors.raise_type_error "wrong argument type"
    and mkTmReturn v ctx =
        print_endline ("(*" ^ "mkTmReturn" ^ "*)");
        mkReturn v (mkTyMonad (typeof_value v)) ctx
    and mkTmLetExpr v e1 e2 ctx =
        print_endline ("(*" ^ "mkTmLetExpr" ^ "*)");
        let e1' = e1 ctx in
        let ctx' = ctx |> extend_vvar v (unpack_monad (typeof e1')) in
        let e2' = e2 ctx' in
        let _, ty1 = unpack_exists (typeof e2') in
        let _, ty2 = unpack_extended ty1 in
        if is_monad ty2
        then mkLetExpr v e1' e2' (typeof e2') ctx
        else Errors.raise_type_error "only monads are allowed"
    and mkTmLetTuple vs x e ctx =
        print_endline ("(*" ^ "mkTmLetTuple" ^ "*)");
        let tys = unpack_tuple (typeof_value x) in
        let ctx' = ctx |> extend_vvars vs tys in
        let e' = e ctx' in
        mkLetTuple vs x e' (typeof e') ctx
    and mkTmLetPack e1 e2 ctx =
        let v = make_mvar () in
        print_endline ("(*" ^ ("mkTmLetPack " ^ string_of_mvar v) ^ "*)");
        let e1' = e1 ctx in
        let vars, ty' = unpack_exists (typeof e1') in
        let vars' = List.map (fun _ -> make_tvar ()) vars in
        let ctx' = ctx |> extend_tvars vars' |> extend_mvar v ty' in
        let e2' = e2 v vars' ctx' in
        let e2'' = if is_mvar_marked v ctx' then e2' else mkWeak v e2' (typeof e2') ctx' in
        mkLetPack vars' v e1' e2'' (typeof e2'') ctx
    and mkTmLetPackNl x e1 e2 ctx =
        let v = make_mvar () in
        print_endline ("(*" ^ ("mkTmLetPackNl " ^ string_of_mvar v) ^ "*)");
        let e1' = e1 ctx in
        let vars, ty' = unpack_exists (typeof e1') in
        let vars' = List.map (fun _ -> make_tvar ()) vars in
        let ctx' = ctx |> extend_tvars vars' |> extend_mvarnl v x ty' in
        let e2' = e2 v vars' ctx' in
        let e2'' = if is_mvar_marked v ctx' then e2' else mkWeak v e2' (typeof e2') ctx' in
        mkLetPack vars' v e1' e2'' (typeof e2'') ctx
    and mkTmLetRecord e1 e2 ctx =
        let e1' = e1 ctx in
        let items = unpack_record (typeof e1') in
        let mvars = List.map (fun _ -> make_mvar ()) items in
        print_endline ("(*" ^ ("mkTmLetRecord " ^ (String.concat "," (List.map2 (fun (l,_) x -> l ^ "->" ^ string_of_mvar x) items mvars))) ^ "*)");
        let labels, tys = List.split items in
        let ctx' = ctx |> extend_mvars mvars tys in
        let vars = List.combine labels mvars in
        let e2' = e2 vars ctx' in
        let unmarked_mvars = List.filter (fun v -> not (is_mvar_marked v ctx')) mvars in
        let e2'' = List.fold_left (fun e v -> mkWeak v e (typeof e) ctx') e2' unmarked_mvars in
        mkLetRecord vars e1' e2'' (typeof e2'') ctx
    and mkTmPack ts' ts e ctx =
        print_endline ("(*" ^ "mkTmPack" ^ "*)");
        let e' = e ctx in
        mkPack ts e' (mkTyExists ts' (typeof e')) ctx
    and mkTmLetOp v x e ctx =
        print_endline ("(*" ^ "mkTmLetOp" ^ "*)");
        let ctx' = ctx |> extend_vvar v (mkTyInt ()) in
        let e' = e ctx' in
        mkLetOp v x e' (typeof e') ctx
    and mkTmMatch x es ctx =
        print_endline ("(*" ^ "mkTmMatch" ^ "*)");
        let tys = unpack_sum (typeof_value x) in
        let aux (i, v, e) ty =
            let ctx' = ctx |> extend_vvar v ty in
            i, v, e ctx' in
        let es' = List.map2 aux es tys in
        mkMatch x (List.map (fun (i, v, e') -> i, v, e') es') (unique_type es') ctx
    and mkTmRecord items ctx =
        print_endline ("(*" ^ "mkTmRecord " ^ (String.concat "," (List.map (fun (l, _) -> l) items)) ^ "*)");
        let items' = List.map (fun (l, e) -> (l, e ctx)) items in
        let ty = (mkTyRecord (List.map (fun (l, e') -> (l, (typeof e'))) items')) in
        mkRecord (List.map (fun (l, e') -> l, e') items') ty ctx
    and mkTmStar t ctx =
        print_endline ("(*" ^ "mkTmStar" ^ "*)");
        mkStar t (mkTyDummy t) ctx
    and mkTmCloseR e ctx =
        print_endline ("(*" ^ "mkTmCloseR" ^ "*)");
        let t = mkTyVar (make_tvar ()) in
        let ctx' = ctx |> extend_vvar (make_vvar ()) t in
        let e' = e ctx' in
        mkCloseR t e' (mkTyExtended t (typeof e')) ctx
    and mkTmCloseRExplicit t e ctx =
        print_endline ("(*" ^ "mkTmCloseR" ^ "*)");
        let ctx' = ctx |> extend_vvar (make_vvar ()) t in
        let e' = e ctx' in
        mkCloseR t e' (mkTyExtended t (typeof e')) ctx
    and mkTmCloseL x e ctx =
        print_endline ("(*" ^ "mkTmCloseL" ^ "*)");
        let ty' = extended_inner (mtypeof x ctx) in
        let ctx' = ctx |> exchange_mvar_with_vvar x ty' in
        let e' = e ctx' in
        mkCloseL e' (typeof e') ctx
    and mkTmContract x xs e ctx =
        print_endline ("(*" ^ ("mkTmContract " ^ string_of_mvar x ^ ": " ^ (String.concat ", " (List.map string_of_mvar xs))) ^ "*)");
        let ty = mtypeof x ctx in
        let ext', ty' = unpack_extended ty in
        let new_exts = unpack_sum ext' in
        let ctx' = ctx |> replace_mvar_with_newly_marked_mvars x xs (List.map (fun ext -> mkTyExtended ext ty') new_exts) in
        mark_mvar x ctx;
        let e' = e ctx' in
        mkContract x xs e' new_exts (typeof e') ctx
    and mkTmContractSub x xs e ctx =
        let new_exts = List.map (fun _ -> mkTyVar (make_tvar ())) xs in
        let ext'' = mkTySum new_exts in
        ctx |> mkTmSubtype x ext'' (mkTmContract x xs e)
    and mkTmDerelict' x e ctx =
        print_endline ("(*" ^ ("mkTmDerelict " ^ string_of_mvar x) ^ "*)");
        let ty = mtypeof x ctx in
        let ext', ty' = unpack_extended ty in
        let ctx' = ctx |> replace_mvar_with_mvars x [x] [ty'] in
        let e' = e ctx' in
        mkDerelict x e' (typeof e') ctx
    and mkTmDerelict x e ctx =
        mkTmSubtype x (mkTyUnit ()) (mkTmDerelict' x e) ctx
    and mkTmWeak x e ctx =
        print_endline ("(*" ^ ("mkTmWeak " ^ string_of_mvar x) ^ "*)");
        let ctx' = ctx |> replace_mvar_with_mvars x [] [] in
        let e' = e ctx' in
        e' 
    and mkTmCloseRInv e ctx =
        print_endline ("(*" ^ ("mkTmCloseRInv") ^ "*)");
        let ctx' = ctx |> drop_last_entry in
        let e' = e ctx' in
        let _, ty' = unpack_extended (typeof e') in
        mkCloseRInv e' ty' ctx
    and mkTmDig x e ctx =
        print_endline ("(*" ^ ("mkTmDig " ^ string_of_mvar x) ^ "*)");
        let ty = mtypeof x ctx in
        let ext', ty' = unpack_extended ty in
        let exts' = unpack_tuple ext' in
        let ty'' = List.fold_right mkTyExtended exts' ty' in
        let ctx' = ctx |> replace_mvar_with_mvars x [x] [ty''] in
        let e' = e ctx' in
        mkDig x e' exts' (typeof e') ctx
    and mkTmSubtype x ext e ctx =
        print_endline ("(*" ^ ("mkTmSubtype " ^ string_of_mvar x) ^ "*)");
        let ty = mtypeof x ctx in
        let ext', ty' = unpack_extended ty in
        assert (compatible_deferred add_constraint ext ext');
        let ctx' = ctx |> replace_mvar_with_mvars x [x] [mkTyExtended ext ty'] in
        let e' = e ctx' in
        mkSubtype x e' (typeof e') ctx
    and mkTmDuplicate x e ctx =
        print_endline ("(*" ^ ("mkTmDuplicate " ^ string_of_mvar x) ^ "*)");
        let ty = mtypeof x ctx in
        assert (not (is_extended ty));
        let ty' = mkTyExtended (mkTyVar (make_tvar ())) ty in
        let ctx' = ctx |> replace_mvar_with_mvars x [x] [ty'] in
        let e' = e ctx' in
        mkDuplicate x e' (typeof e') ctx

end
