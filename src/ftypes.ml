open Types
open Printer


type fbtype = {
    fbtype_desc: fbtype_desc ref;
    fbtype_mark: int
}

and fbtype_desc =
| Link of fbtype
| Path of path
| Unit
| Empty
| Int
| Var of tvar
| VarVar of tvar
| Sum of fbtype list
| Tuple of fbtype list
| Raw of extint ref

let eq_fbtype (t1: fbtype) (t2: fbtype) : bool =
    t1.fbtype_desc = t2.fbtype_desc

let list_extint_sum xs =
    List.fold_left extint_add (Value 0) xs

let list_extint_max xs =
    List.fold_left extint_max (Value 0) xs

let rec sizeof (t1: fbtype) : extint =
    match !(t1.fbtype_desc) with
    | Unit -> Value 1
    | Empty -> Value 0
    | Int -> Value 4
    | Tuple ts -> list_extint_sum (List.map sizeof ts)
    | Sum ts -> list_extint_max (List.map sizeof ts)
    | Raw k -> !k
    | _ -> Errors.raise_internal_error "sizeof: called for unexpected case"

type ftype = {
    ftype_desc: ftype_desc
}

and fitem =
    label * ftype

and ftype_desc =
| Monad of fbtype
| Lolli of ftype * ftype
| Arrow of fbtype * ftype
| Forall of tvar list * ftype
| Exists of tvar list * ftype
| Extended of fbtype * ftype
| Record of fitem list
| Dummy of fbtype

let rec pp_ftype t p =
    match t.ftype_desc with
    | Monad t -> p |> enter "ftype.Monad" |> dumpv pp_fbtype t |> leave
    | Lolli (t1, t2) -> p |> enter "ftype.Lolli" |> dumpkv "t1" pp_ftype t1 |> dumpkv "t2" pp_ftype t2 |> leave
    | Arrow (t1, t2) -> p |> enter "ftype.Arrow" |> dumpkv "t1" pp_fbtype t1 |> dumpkv "t2" pp_ftype t2 |> leave
    | Forall (vs, t) -> p |> enter "ftype.Forall" |> dumps (String.concat "," (List.map string_of_tvar vs)) |> dumpv pp_ftype t |> leave
    | Exists (vs, t) -> p |> enter "ftype.Exists" |> dumps (String.concat "," (List.map string_of_tvar vs)) |> dumpv pp_ftype t |> leave
    | Extended (t1, t2) -> p |> enter "ftype.Extended" |> dumpv pp_fbtype t1 |> dumpv pp_ftype t2 |> leave
    | Record items -> p |> enter "ftype.Record" |> dumpvs pp_fitem items |> leave
    | Dummy t -> p |> enter "ftype.Dummy" |> dumpv pp_fbtype t |> leave

and pp_fitem (l, t) p =
    p |> enterk (string_of_label l) "fitem" |> dumpv pp_ftype t |> leave

and pp_fbtype t p =
    match !(t.fbtype_desc) with
    | Path p' -> dumpl (string_of_path p') "fbtype.Path" p
    | Unit -> dumpl "Unit" "fbtype.Unit" p
    | Empty -> dumpl "Empty" "fbtype.Empty" p
    | Int -> dumpl "Int" "fbtype.Int" p
    | Var v -> dumpl (string_of_tvar v) "fbtype.Var" p
    | VarVar v -> dumpl (string_of_tvar v) "fbtype.VarVar" p
    | Sum ts -> p |> enter "fbtype.Sum" |> dumpvs pp_fbtype ts |> leave
    | Tuple ts -> p |> enter "fbtype.Tuple" |> dumpvs pp_fbtype ts |> leave
    | Raw n -> dumpl (Printf.sprintf "Raw(%s)" (string_of_extint !n)) "fbtype.Raw" p
    | Link t' -> pp_fbtype t' p

let mkfbType desc = { fbtype_desc = ref desc; fbtype_mark = 0 }

let mkTyEmpty () = mkfbType Empty
let mkTyPath p = mkfbType (Path p)
let mkTyUnit () = mkfbType Unit
let mkTyInt () = mkfbType Int
let mkTyVar v = mkfbType (Var v)
let mkTySum ts = mkfbType (Sum ts)
let mkTyTuple ts = mkfbType (Tuple ts)
let mkTyRaw n = mkfbType (Raw n)
let mkTyLink t = mkfbType (Link t)
let mkTyVarVar v = mkfbType (VarVar v)

let mkfType desc = { ftype_desc = desc }

let mkTyMonad t = mkfType (Monad t)
let mkTyExists vs t = mkfType (Exists (vs, t))
let mkTyDummy t = mkfType (Dummy t)
let mkTyRecord items = mkfType (Record items)
let mkTyExtended t1 t2 = mkfType (Extended (t1, t2))
let mkTyForall vs t = mkfType (Forall (vs, t))
let mkTyArrow t1 t2 = mkfType (Arrow (t1, t2))
let mkTyLolli t1 t2 = mkfType (Lolli (t1, t2))

let unpack_exists (t: ftype) : tvar list * ftype =
    match t.ftype_desc with
    | Exists (vs', t') -> vs', t'
    | _ -> Errors.raise_type_error "expected exists"

let unpack_exists_vars (t: ftype) : tvar list =
    match t.ftype_desc with
    | Exists (vs', t') -> vs'
    | _ -> Errors.raise_type_error "expected exists"

let unpack_forall_vars (t: ftype) : tvar list =
    match t.ftype_desc with
    | Forall (vs', _) -> vs'
    | _ -> Errors.raise_type_error "expected forall"

let unpack_forall (t: ftype) : tvar list * ftype =
    match t.ftype_desc with
    | Forall (vs', ty) -> vs', ty
    | _ -> Errors.raise_type_error "expected forall"

let unpack_functor (t: ftype) : tvar list * ftype * ftype =
    match t.ftype_desc with
    | Forall (vars, t') ->
        (match t'.ftype_desc with
        | Lolli (t1, t2) -> vars, t1, t2
        | _ -> assert false)
    | _ -> Errors.raise_type_error "expected functor"

let unpack_dummy (t: ftype) : fbtype =
    match t.ftype_desc with
    | Dummy t -> t
    | _ -> Errors.raise_type_error "expected type declaration"

let is_dummy (t: ftype) : bool =
    match t.ftype_desc with
    | Dummy _ -> true
    | _ -> false

let unpack_extended (t: ftype) : fbtype * ftype =
    match t.ftype_desc with
    | Extended (b, t') -> b, t'
    | _ -> Errors.raise_type_error "expected type annotation"

let is_extended (t: ftype) : bool =
    match t.ftype_desc with
    | Extended _ -> true
    | _ -> false

let extended_inner (t: ftype) : ftype =
    match t.ftype_desc with
    | Extended (_, t') -> t'
    | _ -> Errors.raise_type_error "expected type annotation"

let unpack_monad (t: ftype) : fbtype =
    match t.ftype_desc with
    | Monad t' -> t'
    | _ -> Errors.raise_type_error "expected monad"

let record_typeof l t =
    match t.ftype_desc with
    | Record items -> (try List.assoc l items with _ -> Errors.raise_type_error (Printf.sprintf "unknown member %s" l))
    | _ -> Errors.raise_type_error "expected record"

let unpack_arrow (t: ftype) : fbtype * ftype =
    match t.ftype_desc with
    | Arrow (t1, t2) -> t1, t2
    | _ -> Errors.raise_type_error "expected function"

let is_arrow (t: ftype) : bool =
    match t.ftype_desc with
    | Arrow _ -> true
    | _ -> false

let arrow_result_type (t: ftype) : ftype =
    match t.ftype_desc with
    | Arrow (_, t2) -> t2
    | _ -> Errors.raise_type_error "expected function"

let is_monad (t: ftype) : bool =
    match t.ftype_desc with
    | Monad _ -> true
    | _ -> false

let unpack_lolli (t: ftype) : ftype * ftype =
    match t.ftype_desc with
    | Lolli (t1, t2) -> t1, t2
    | _ -> Errors.raise_type_error "expected functor"

let unpack_forall_inner (t: ftype) : ftype =
    match t.ftype_desc with
    | Forall (_, t') -> t'
    | _ -> Errors.raise_type_error "expected forall"

let lolli_result_type (t: ftype) : ftype =
    match t.ftype_desc with
    | Lolli (_, t2) -> t2
    | _ -> Errors.raise_type_error "expected functor"

let rec unpack_til_dummy_or_record (t: ftype) : ftype =
    match t.ftype_desc with
    | Extended (_, t') -> unpack_til_dummy_or_record t'
    | Dummy t' -> t
    | Record _ -> t
    | _ -> Errors.raise_type_error "excpected record or type declaration"

let unpack_tuple (t: fbtype) : fbtype list =
    match !(t.fbtype_desc) with
    | Tuple ts -> ts
    | _ -> Errors.raise_type_error "expected product"

let unpack_sum (t: fbtype) : fbtype list =
    match !(t.fbtype_desc) with
    | Sum ts -> ts
    | _ -> Errors.raise_type_error "expected sum"

let unpack_record (t: ftype) : fitem list =
    match t.ftype_desc with
    | Record items -> items
    | _ -> Errors.raise_type_error "expected record"

let unpack_record_items (t: ftype) : label list =
    match t.ftype_desc with
    | Record items -> let labels = List.map (fun (l, _) -> l) items in labels
    | _ -> Errors.raise_type_error "expected record"

let is_int (t: fbtype) =
    match !(t.fbtype_desc) with
    | Int -> true
    | _ -> false


let compare_1st_of_pair x y =
    match x, y with (x1, _), (y1, _) -> compare x1 y1 

let record_normalize_items (ts: fitem list) : fitem list =
    List.sort compare_1st_of_pair ts

and prepare_ftype_for_inference (vars: tvar list) (t: ftype) : fbtype list * ftype =
    let vvars = List.map (fun v -> v, mkTyVarVar v) vars in
    let rec instantiate_ftype (t: ftype) : ftype =
        match t.ftype_desc with
        | Monad t' -> mkTyMonad (instantiate_fbtype t')
        | Lolli (t1, t2) -> mkTyLolli (instantiate_ftype t1) (instantiate_ftype t2)
        | Arrow (t1, t2) -> mkTyArrow (instantiate_fbtype t1) (instantiate_ftype t2)
        | Forall (vs, t') -> mkTyForall vs (instantiate_ftype t')
        | Exists (vs, t') -> mkTyExists vs (instantiate_ftype t')
        | Extended (t1, t2) -> mkTyExtended (instantiate_fbtype t1) (instantiate_ftype t2)
        | Record items -> mkTyRecord (List.map (fun (l, t') -> l, instantiate_ftype t') items)
        | Dummy t' -> mkTyDummy (instantiate_fbtype t')
    and instantiate_fbtype (t: fbtype) : fbtype =
        match !(t.fbtype_desc) with
        | Path _
        | Raw _
        | Unit
        | Int
        | Empty -> t
        | Sum ts -> mkTySum (List.map instantiate_fbtype ts)
        | Tuple ts -> mkTyTuple (List.map instantiate_fbtype ts)
        | Link t' -> mkTyLink (instantiate_fbtype t')
        | Var v -> (match List.assoc_opt v vvars with Some t' -> t' | None -> t) 
        | VarVar _ -> Errors.raise_internal_error "prepare_ftype_for_inference: called for unepected case"
    in List.map (fun (_, x) -> x) vvars, instantiate_ftype t

and finish_ftype_after_inference (vvars: fbtype list) : unit =
    List.iter
        (fun v -> match !(v.fbtype_desc) with VarVar _ -> v.fbtype_desc := Unit | _ -> ())
        vvars

let subs_tvar (sub: (tvar * fbtype) list) (t: tvar) : fbtype =
    match List.assoc_opt t sub with
    | Some t' -> t'
    | None -> mkTyVar t

let rec subs_fbtype (sub: (tvar * fbtype) list) (t: fbtype): fbtype =
    match !(t.fbtype_desc) with
    | Var v -> subs_tvar sub v
    | Tuple vs -> mkTyTuple (List.map (subs_fbtype sub) vs)
    | Sum vs -> mkTySum (List.map (subs_fbtype sub) vs)
    | _ -> t

let rec subs_ftype (sub: (tvar * fbtype) list) (t: ftype): ftype =
    match t.ftype_desc with
    | Monad v -> mkTyMonad (subs_fbtype sub v)
    | Lolli (v, w) -> mkTyLolli (subs_ftype sub v) (subs_ftype sub w)
    | Arrow (v, w) -> mkTyArrow (subs_fbtype sub v) (subs_ftype sub w)
    | Forall (vs, w) -> mkTyForall vs (subs_ftype sub w)
    | Exists (vs, w) -> mkTyExists vs (subs_ftype sub w)
    | Extended (v, w) -> mkTyExtended (subs_fbtype sub v) (subs_ftype sub w)
    | Record vs -> mkTyRecord (List.map (fun (l, v) -> (l, subs_ftype sub v)) vs)
    | Dummy v -> mkTyDummy (subs_fbtype sub v)





let rec find (t: fbtype) : fbtype =
    match !(t.fbtype_desc) with
    | Link t' ->
        let r = find t' in
        t.fbtype_desc := Link r;
        r
    | _ -> t

and union (t1: fbtype) (t2: fbtype) : unit =
    t1.fbtype_desc := Link t2

and unify_varvar_only (t1: fbtype) (t2: fbtype) : unit =
    let r1, r2 = find t1, find t2 in
    if r1 == r2 then () else
    match !(r1.fbtype_desc), !(r2.fbtype_desc) with
    | VarVar _, _ -> union r1 r2
    | _, VarVar _ -> union r2 r1
    | Var _, Var _
    | Unit, Unit 
    | Empty, Empty
    | Int, Int -> ()
    | Raw n1, Raw n2 when n1 = n2 -> ()
    | Tuple xs, Tuple ys
    | Sum xs, Sum ys ->
        union r1 r2;
        List.iter2 unify_varvar_only xs ys
    | _ -> Errors.raise_type_error "incompatible types"

and unify_var_only (t1: fbtype) (t2: fbtype) : unit =
    let r1, r2 = find t1, find t2 in
    if r1 == r2 then () else
    match !(r1.fbtype_desc), !(r2.fbtype_desc) with
    | Var _, _ -> union r1 r2
    | _, Var _ -> union r2 r1
    | Unit, Unit 
    | Empty, Empty
    | Int, Int -> ()
    | Raw n1, Raw n2 when n1 = n2 -> ()
    | Tuple xs, Tuple ys
    | Sum xs, Sum ys ->
        union r1 r2;
        List.iter2 unify_var_only xs ys
    | _ -> Errors.raise_type_error "incompatible types"

let is_var t =
    match !((find t).fbtype_desc) with
    | Var _ -> true
    | _ -> false

let rec compatible (t1: fbtype) (t2: fbtype) : bool =
    match !(t1.fbtype_desc), !(t2.fbtype_desc) with
    | Unit, Unit -> true
    | Int, Int -> true
    | Empty, Empty -> true
    | Tuple ts, Tuple ts' -> List.fold_left2 (fun b t t' -> b && compatible t t') true ts ts'
    | Sum ts, Sum ts' -> List.fold_left2 (fun b t t' -> b && compatible t t') true ts ts'
    | Raw k, Raw k' -> extint_compare !k !k' <= 0
    | Tuple ts, Raw k -> extint_compare (sizeof t1) !k <= 0
    | Sum ts, Raw k -> extint_compare (sizeof t1) !k <= 0
    | _ -> false

let rec compatible_deferred (add_constraint: fbtype -> fbtype -> unit) (t1: fbtype) (t2: fbtype) : bool =
    match !((find t1).fbtype_desc), !((find t2).fbtype_desc) with
    (*| Unit, Unit -> true
    | Int, Int -> true
    | Empty, Empty -> true
    | Tuple ts, Tuple ts' -> List.fold_left2 (fun b t t' -> b && compatible t t') true ts ts'
    | Sum ts, Sum ts' -> List.fold_left2 (fun b t t' -> b && compatible t t') true ts ts'*)
    | _, Raw k' ->
        assert (extint_compare Infty !k' == 0); true
        (*extint_compare (sizeof t1) !k' <= 0*)
    | _, Var x ->
        unify_var_only (mkTyRaw (ref Infty)) t2; true
        (*add_constraint t1 t2; true*)
    | _, _ -> false

let rec unify_extended (t1: ftype) (t2: ftype) : unit =
    match t1.ftype_desc, t2.ftype_desc with
    | Lolli (t11, t12), Lolli (t21, t22) -> unify_extended t11 t21; unify_extended t12 t22
    | Arrow (t11, t12), Arrow (t21, t22) -> unify_var_only t11 t21; unify_extended t12 t22
    | Exists (_, t1'), Exists (_, t2')
    | Forall (_, t1'), Forall (_, t2') -> unify_extended t1' t2'
    | Extended (t11, t12), Extended (t21, t22) -> unify_extended t12 t22; unify_var_only t11 t21
    | Record i1, Record i2 -> List.iter2 (fun (_, t1') (_, t2') -> unify_extended t1' t2') i1 i2
    | Dummy t1', Dummy t2'
    | Monad t1', Monad t2' -> unify_var_only t1' t2'
    | _ -> Errors.raise_type_error "types not equal"
