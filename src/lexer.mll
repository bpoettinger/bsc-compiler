{
    open Parser
    open Lexing

    exception InvalidChar of char * int * Lexing.position
}

let whitespace = [' ' '\t' '\n' '\r']
let identifier_upper = ['A'-'Z']['a'-'z' 'A'-'Z' '_' '0'-'9']*
let identifier_lower = ['a'-'z']['a'-'z' 'A'-'Z' '_' '0'-'9']*
let integer = '0' | ['1'-'9']['0'-'9']*

rule main = parse
| "unit"            { UNIT }
| "int"             { INT }
| "("               { LPAREN }
| ")"               { RPAREN }
| "="               { EQUAL }
| "+"               { PLUS }
| "-"               { MINUS }
| "*"               { TIMES }
| ","               { COMMA }
| "."               { DOT }
| ":"               { COLON }
| ";"               { SEMICOLON }
| ":>"              { SEAL }
| "->"              { ARROW1 }
| "=>"              { ARROW2 }
| "let"             { LET }
| "in"              { IN }
| "case"            { CASE }
| "of"              { OF }
| "fn"              { FN }
| "as"              { AS }
| "functor"         { FUNCTOR }
| "end"             { END }
| "sig"             { SIG }
| "struct"          { STRUCT }
| "type"            { TYPE }
| "inl"             { INL }
| "inr"             { INR }
| "C" ((['1'-'9']['0'-'9']*) as i) { CI (int_of_string i) }
| "return"          { RETURN }
| "monad"           { MONAD }
| "%"               { PERCENT }
| "/"               { SLASH }
| "<"               { LABRACK }
| ">"               { RABRACK }
| integer      as x { NUMBER (int_of_string x) } 
| identifier_lower as x { IDENTIFIER_LOWER x } 
| identifier_upper as x { IDENTIFIER_UPPER x } 
| "\n"              { new_line lexbuf; main lexbuf }
| whitespace        { main lexbuf }
| eof               { EOF }
| _            as x { raise (InvalidChar (x, Lexing.lexeme_start lexbuf, Lexing.lexeme_start_p lexbuf)) }
