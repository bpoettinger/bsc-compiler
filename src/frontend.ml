open Types
open Mtypes
open Ftypes

module Make (Backend: Backend.BACKEND) = struct

    module Middleend = Middleend.Make (Backend)
    open Middleend

    let fail_on_mvar_shadow v defs = match List.find_opt (mvar_eq v) defs with None -> () | _ -> Errors.raise_context_error (Printf.sprintf "Shadowed mvar '%s'" (string_of_mvar v))
    let fail_on_vvar_shadow v vdefs = match List.find_opt (fun v' -> v = v') vdefs with None -> () | _ -> Errors.raise_context_error (Printf.sprintf "Shadowed vvar '%s'" (string_of_vvar v))

    let find_def p defs = List.find (mvar_eq (path_mvar p)) defs
    let register_def v defs = fail_on_mvar_shadow v defs; v :: defs
    let register_vdef v vdefs = fail_on_vvar_shadow v vdefs; v :: vdefs
    let inst_def p defs = instantiate_path p (find_def p defs) (make_mvar ())

    let linearize_mterm term =
        let rec aux term defs vdefs =
            let rec aux' (term: mterm) (defs: mvar list) (vdefs: vvar list) : unit =
                match term.mterm_desc with
                | Path p ->
                    inst_def p defs;
                | Struct items -> 
                    let _ = List.fold_left (fun defs item -> aux item.term defs vdefs; register_def item.var defs) defs items in
                    ()
                | Functor (v, t, e) ->
                    let defs' = register_def v defs in
                    aux e defs' vdefs
                | Function (v, t, e) ->
                    aux e defs (register_vdef v vdefs)
                | FunctorApp (e1, e2) ->
                    aux e1 defs vdefs;
                    inst_def e2 defs
                | FunctionApp (e1, _) ->
                    aux e1 defs vdefs
                | Seal (e1, t) ->
                    aux e1 defs vdefs
                | LetOp (v, _, e) ->
                    aux e defs (register_vdef v vdefs)
                | LetExpr (v, e1, e2) ->
                    let vdefs' = register_vdef v vdefs in
                    aux e1 defs vdefs;
                    aux e2 defs vdefs'
                | LetTuple (vs, _, e) ->
                    let vdefs' = List.fold_left (fun vdefs' v -> register_vdef v vdefs') vdefs vs in
                    aux e defs vdefs'
                | Match (_, cs) ->
                    List.iter (fun (i, v, e) -> aux e defs (register_vdef v vdefs)) cs
                | _ -> ()
            in try aux' term defs vdefs with Errors.Error e ->
                Errors.print_error e term.mterm_loc;
                Printexc.print_backtrace stdout;
                Errors.raise_abort ()
        in aux term [] []





    let elaborate_mtype_closer (f: context -> ftype) (ctx: context) =
        let new_tvar = mkTyVar (make_tvar ()) in
        let ctx' = ctx |> extend_vvar (make_vvar ()) new_tvar in
        mkTyExtended new_tvar (f ctx')
    
    let rec elaborate_mbtype' (btype: mbtype) (ctx: context) : fbtype =
        match btype.btype_desc with
        | Empty -> mkTyEmpty ()
        | Unit -> mkTyUnit ()
        | Int -> mkTyInt ()
        | Tuple types -> mkTyTuple (List.map (fun t -> elaborate_mbtype t ctx) types)
        | Sum types -> mkTySum (List.map (fun t -> elaborate_mbtype t ctx) types)
        | Path p ->
            let v, ls = unpack_path p in
            let rec aux (t: ftype) (ls: label list) : ftype =
                match ls with
                | [] -> t
                | l::ls' -> aux (unpack_til_dummy_or_record (record_typeof l t)) ls' in
            unpack_dummy (aux (unpack_til_dummy_or_record (mnltypeof v ctx)) ls)
    
    and elaborate_mbtype ty ctx =
        try
            elaborate_mbtype' ty ctx
        with Errors.Error e ->
            Errors.print_error e ty.btype_loc;
            Printexc.print_backtrace stdout;
            Errors.raise_abort ()
    
    let rec elaborate_mtype' (mtype: mtype) (ctx: context) : ftype =
        match mtype.mtype_desc with
        | Abstract -> 
            let new_var = make_tvar () in
            mkTyExists [new_var] (elaborate_mtype_closer (fun _ -> mkTyDummy (mkTyVar new_var)) ctx)
        | Concrete t -> 
            mkTyExists [] (elaborate_mtype_closer (fun _ -> mkTyDummy (elaborate_mbtype t ctx)) ctx)
        | Signature ds -> 
            let vars = ref [] in
            let f ctx =
                let aux (items, vars, ctx) (decl: mdecl) =
                    let vars1, s' = unpack_exists (elaborate_mtype decl.ty ctx) in
                    let ctx' = ctx |> extend_mvarnl decl.var decl.var s' |> extend_tvars vars1 in
                    ((decl.name, s') :: items, vars1 @ vars, ctx') in
                let items', vars', ctx' = List.fold_left aux ([], [], ctx) ds in
                vars := vars';
                mkTyRecord items'
            in mkTyExists !vars (elaborate_mtype_closer f ctx)
        | Functor (l, s, t) -> 
            let f ctx =
                let vars1, s' = unpack_exists (elaborate_mtype s ctx) in
                let ctx' = ctx |> extend_mvar l s' |> extend_tvars vars1 in
                let vars2, t' = unpack_exists (elaborate_mtype t ctx') in
                mkTyForall vars1 (mkTyLolli s' (mkTyExists vars2 t'))
            in mkTyExists [] (elaborate_mtype_closer f ctx)
        | Function (s, t) ->
            let f ctx =
                let s' = elaborate_mbtype s ctx in
                let ctx' = ctx |> extend_vvar (make_vvar ()) s' in
                let t' = elaborate_mtype t ctx' in
                mkTyArrow s' t'
            in mkTyExists [] (elaborate_mtype_closer f ctx)
        | Monad b ->
            mkTyExists [] (elaborate_mtype_closer (fun _ -> mkTyMonad (elaborate_mbtype b ctx)) ctx)
    
    and elaborate_mtype mtype ctx =
        try
            elaborate_mtype' mtype ctx
        with Errors.Error e ->
            Errors.print_error e mtype.mtype_loc;
            Printexc.print_backtrace stdout;
            Errors.raise_abort ()
    
    let rec elaborate_mvalue' (v: mvalue) (ctx: context) : value =
        match v.mvalue_desc with
        | Var v -> mkValVar v (vtypeof v ctx)
        | Unit -> mkValUnit ()
        | Int x -> mkValInt x
        | Tuple xs -> mkValTuple (List.map (fun x -> elaborate_mvalue x ctx) xs)
        | Sum (i, x, tys) ->
            let x' = elaborate_mvalue x ctx in
            let tys' = List.map (fun ty -> elaborate_mbtype ty ctx) tys in
            if eq_fbtype (typeof_value x') (List.nth tys' i)
            then mkValSum i x' tys'
            else Errors.raise_type_error "wrong type annotation for sum value"
        | Op (v1, op, v2) ->
            let v1' = elaborate_mvalue v1 ctx in
            let v2' = elaborate_mvalue v2 ctx in
            if eq_fbtype (typeof_value v1') (mkTyInt ()) && eq_fbtype (typeof_value v2') (mkTyInt ())
            then mkValOp v1' op v2' (mkTyInt ())
            else Errors.raise_type_error "arithmetic operations expect integers"
    
    and elaborate_mvalue v ctx =
        try
            elaborate_mvalue' v ctx
        with Errors.Error e ->
            Errors.print_error e v.mvalue_loc;
            Printexc.print_backtrace stdout;
            Errors.raise_abort ()

    let rec elaborate_type_xe (t1: ftype) (t2: ftype) (ctx: context) =
        let vars2, t2' = unpack_exists t2 in
        let ass, t2'' = prepare_ftype_for_inference vars2 t2' in
        let coerc = elaborate_type_xx t1 t2'' ctx in
        let () = finish_ftype_after_inference ass in
        vars2, ass, coerc

    and elaborate_type_ee (t1: ftype) (t2: ftype) (ctx: context) =
        let vars1, t1' = unpack_exists t1 in
        let ctx' = ctx |> extend_tvars vars1 in
        let vars, ass, coerc = elaborate_type_xe t1' t2 ctx' in
        ctx |> mkTmFabs t1 (fun new_mvar_x ->
            mkTmLetPack (mkTmVar new_mvar_x) (fun new_mvar_y vars1 ->
                mkTmPack vars ass (
                    mkTmFapp (fun _ -> coerc) (mkTmVar new_mvar_y))))

    and elaborate_type_xx (t1: ftype) (t2: ftype) (ctx: context) =
        match t1.ftype_desc, t2.ftype_desc with
        | Dummy t1', Dummy t2' ->
            unify_varvar_only t1' t2';
            ctx |> mkTmFabs t1 mkTmVar
        | Monad t1', Monad t2' ->
            unify_varvar_only t1' t2';
            ctx |> mkTmFabs t1 mkTmVar
        | Extended (t10, {ftype_desc = Arrow (t11, t12)}), Extended (t20, {ftype_desc = Arrow (t21, t22)}) ->
            unify_varvar_only t11 t21;
            let new_var_x = make_vvar () in
            let new_var_x' = make_vvar () in
            let ctx' = ctx |> extend_vvar new_var_x' t20 |> extend_vvar new_var_x t11 in
            let coerc = elaborate_type_ee t12 t22 ctx' in
            ctx |> mkTmFabs t1 (fun new_mvar_f ->
                mkTmCloseRExplicit t20 (
                    mkTmBabs new_var_x t11 (
                        mkTmFapp (fun _ -> coerc) (
                            mkTmBapp (mkTmVarInner new_mvar_f)
                                (mkValVar new_var_x t11)))))
        | Extended (t11, t12), Extended (t21, t22) ->
            unify_var_only t11 t21;
            ctx |> mkTmFabs t1 (fun new_mvar ->
                mkTmCloseR (
                    mkTmCloseL new_mvar (
                        mkTmFapp (elaborate_type_xx t12 t22) (mkTmVar new_mvar))))
        | Forall (vars1, t1'), Forall (vars2, t2') ->
            let t11, t12 = unpack_lolli t1' in
            let t21, t22 = unpack_lolli t2' in
            let ctx' = ctx |> extend_tvars vars2 in
            let vars, ass, coerc1 = elaborate_type_xe t21 (mkTyExists vars1 t11) ctx' in
            let coerc2 = elaborate_type_ee t12 t22 ctx' in
            ctx |> mkTmFabs (mkTyForall vars1 (mkTyLolli t11 t12)) (fun new_mvar_f ->
                mkTmTabs vars2 ( 
                    mkTmFabs t21 (fun new_mvar_x ->
                        mkTmFapp (fun _ -> coerc2) (
                            mkTmFapp
                                (mkTmTapp (mkTmVarOuter new_mvar_f) ass)
                                (mkTmFapp (fun _ -> coerc1) (mkTmVarOuter new_mvar_x))))))
        | Record items1, Record items2 ->
            let rec it (xs': fitem list) (ys': fitem list) (vars: (label * mvar) list) : (label * fterm) list = 
                match xs', ys' with
                | _, [] -> []
                | [], (l,_)::_ -> failwith ""
                | (l,v)::xs, (l',v')::ys ->
                    match compare l l' with
                    | 0 ->
                        let coerc = fun _ -> elaborate_type_xx v v' ctx in
                        let items = it xs ys vars in
                        (l, mkTmFapp coerc (mkTmVarOuter (List.assoc l vars))) :: items
                    | -1 -> it xs ((l',v') :: ys) vars
                    | 1 -> failwith ""
                    | _ -> assert false in
            let items1' = record_normalize_items items1 in
            let items2' = record_normalize_items items2 in
            let new_items = it items1' items2' in
            ctx |> mkTmFabs t1 (fun new_var_x ->
                mkTmLetRecord (mkTmVar new_var_x) (fun vars ->
                    mkTmRecord (new_items vars)))
        | _ -> Errors.raise_type_error "incompatible types"



    type tree = Tree of (label * tree) list * mvar list
    let empty_tree = Tree ([], [])

    let rec tree_insert (v: mvar) ls (Tree (children, instances): tree) =
        match ls with 
        | [] -> Tree (children, v :: instances)
        | l::ls' ->
            match List.assoc_opt l children with
            | Some t -> Tree ((l, tree_insert v ls' t) :: (List.remove_assoc l children), instances)
            | None -> Tree ([(l, tree_insert v ls' empty_tree)], instances)
    
    let build_instance_tree (v: mvar) : tree =
        List.fold_left (fun t (p, i) -> tree_insert i (path_labels p) t) empty_tree v.instances
    
    let rec create_instances (x: mvar) (Tree (children, instances): tree) (e: context -> t) (ctx: context) =
        if (List.length instances + List.length children) = 0
        then ctx |> e
        else
            if (List.length children) = 0
            then ctx |> mkTmContractSub x instances e
            else 
                let y = make_mvar () in
                let instances' = y :: instances in
                ctx |> mkTmContractSub x instances' (mkTmDerelict y (mkTmLetRecord (mkTmVar y) (fun items -> 
                    List.fold_left (fun e (l, t) -> create_instances (List.assoc l items) t e) e children)))
    
    and elaborate_mterm' term ctx =
        match term.mterm_desc with
        | Path p ->
            let v = path_unique_mvar p in
            ctx |> mkTmPack [] [] (mkTmVarInnerBanged v)
        | Type t ->
            let t' = elaborate_mbtype t ctx in
            ctx |> mkTmPack [] [] (mkTmCloseR (mkTmStar t'))
        | Function (x, t, e) -> 
            let t' = elaborate_mbtype t ctx in
            ctx |> mkTmPack [] [] (mkTmCloseR (mkTmBabs x t' (elaborate_mterm e)))
        | FunctionApp (s, v) ->
            let s' = elaborate_mterm s in
            let v' = elaborate_mvalue v ctx in
            ctx |> mkTmLetPack s' (fun mvar tvars -> assert (tvars = []);
                        mkTmDerelict mvar
                            (mkTmBapp (mkTmVar mvar) v'))
        | Functor (v, t, e) ->
            let t' = elaborate_mtype t ctx in
            let vars, t'' = unpack_exists t' in
            let e' v' = create_instances v' (build_instance_tree v) (elaborate_mterm e) in
            ctx |> mkTmPack [] [] (mkTmCloseR (mkTmTabs vars (mkTmFabsNl v t'' e')))
        | FunctorApp (e, v) ->
            let e' = elaborate_mterm e in
            let v' = path_unique_mvar v in
            let ty3 = mtypeof v' ctx in
            ctx |> mkTmLetPack e' (fun mvar tvars -> assert (tvars = []);
                        mkTmDerelict mvar (fun ctx' ->
                            let vars', ty1, ty2 = unpack_functor (mtypeof mvar ctx') in
                            let vars, ass, coerc = elaborate_type_xe ty3 (mkTyExists vars' ty1) ctx in
                            ctx' |> mkTmFapp (mkTmTapp (mkTmVar mvar) ass) (mkTmFapp (fun _ -> coerc) (mkTmVarInnerBanged v'))))
        | Return v -> 
            let v' = elaborate_mvalue v ctx in
            ctx |> mkTmPack [] [] (mkTmCloseR (mkTmReturn v'))
        | LetExpr (x, e1, e2) ->
            ctx |> mkTmLetPack (elaborate_mterm e1) 
                (fun new_mvar vars -> assert (vars = []); mkTmDerelict new_mvar (mkTmLetExpr x (mkTmVar new_mvar) (elaborate_mterm e2)))
        | LetTuple (vs, x, e) -> 
            let x' = elaborate_mvalue x ctx in
            ctx |> mkTmLetTuple vs x' (elaborate_mterm e)
        | Match (x, cs) ->
            let x' = elaborate_mvalue x ctx in
            let cs' = List.map (fun (i, v, e) -> i, v, elaborate_mterm e) cs in
            ctx |> mkTmMatch x' cs'
        | LetOp (v, x, e) -> 
            let x' = elaborate_mvalue x ctx in
            ctx |> mkTmLetOp v x' (elaborate_mterm e)
        | Struct defs ->
            let vars = ref [] in
            let rec aux (defs: mdef list) (items: (label * fterm) list) =
                match defs with
                | [] ->
                    mkTmRecord items
                | def :: defs' ->
                    mkTmLetPackNl def.var (elaborate_mterm def.term) (fun name vars' -> 
                        vars := vars' @ !vars;
                        let name2 = make_mvar () in
                        let instance_tree = build_instance_tree def.var in
                        let instance_tree' = tree_insert name2 [] instance_tree in
                        create_instances name instance_tree' (
                            aux defs' (items @ [(def.name, mkTmVarOuter name2)])))
            in
                let vars' = List.map (fun x -> mkTyVar x) !vars in
                ctx |> mkTmPack !vars vars' (mkTmCloseR (aux defs []))

        | Seal (e1, t2) -> 
            let e1' = elaborate_mterm e1 ctx in
            let t1' = typeof e1' in
            let t2' = elaborate_mtype t2 ctx in
            let coerc = elaborate_type_ee t1' t2' ctx in
            ctx |> mkTmFapp (fun _ -> coerc) (fun _ -> e1')
    
    and elaborate_mterm (term: mterm) (ctx: context) : t =
        try 
            elaborate_mterm' term ctx
        with Errors.Error e ->
            Errors.print_error e term.mterm_loc;
            Printexc.print_backtrace stdout;
            Errors.raise_abort ()

end
