
type error =
| TypeError of string
| ContextError of string
| SyntaxError of string
| InternalError of string

exception Error of error
exception CompiliationFailed

let raise_type_error desc = raise (Error (TypeError desc))
let raise_context_error desc = raise (Error (ContextError desc))
let raise_syntax_error desc = raise (Error (SyntaxError desc))
let raise_internal_error desc = raise (Error (InternalError desc))
let raise_abort () = raise CompiliationFailed

let print_error (err: error) (loc: Lexing.position * Lexing.position) =
    let name, desc = match err with
    | TypeError desc -> "type error", desc
    | ContextError desc -> "context error", desc
    | SyntaxError desc -> "syntax error", desc
    | InternalError desc -> "internal error", desc
    in let loc_begin, loc_end = loc in
    Printf.printf "\n\n================================================================================\n";
    Printf.printf "%s at %s(%d:%d - %d:%d): %s\n" name loc_begin.pos_fname
        loc_begin.pos_lnum (loc_begin.pos_cnum - loc_begin.pos_bol)
        loc_end.pos_lnum (loc_end.pos_cnum - loc_end.pos_bol)
        desc;
    Printf.printf "================================================================================\n\n"
