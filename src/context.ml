open Types
open Ftypes
open Printer

type mark = int ref

type entry = 
| VVar of vvar * fbtype
| MVar of mvar * ftype * mark
| MVarNl of mvar * mvar * ftype * mark
| TVar of tvar

type context = entry list

let pp_entry e p =
    match e with
    | VVar (v, t) -> p |> enter (string_of_vvar v) |> dumpv pp_fbtype t |> leave
    | MVar (v, t, _) -> p |> enter (string_of_mvar v) |> dumpv pp_ftype t |> leave
    | MVarNl (v, v', t, _) -> p |> enter (Printf.sprintf "%s(%s)" (string_of_mvar v) (string_of_mvar v')) |> dumpv pp_ftype t |> leave
    | TVar t -> p |> dumps (string_of_tvar t)

let pp_context ctx p =
    List.fold_right pp_entry ctx p

let empty_context = []

let find_mvar (v: mvar) (ctx: context) : entry option =
    let aux e = (match e with
        | MVar (v', _, _) when mvar_eq v v' -> true
        | MVarNl (v', _, _, _) when mvar_eq v v' -> true
        | _ -> false) in
    List.find_opt aux ctx

let find_mvarnl v ctx =
    let aux e = (match e with
        | MVarNl (_, v', _, _) when mvar_eq v v' -> true
        | _ -> false) in
    List.find_opt aux ctx

let extend_mvar (v: mvar) (t: ftype) (ctx: context): context =
    match find_mvar v ctx with
    | None -> (MVar (v, t, ref 0)) :: ctx
    | _ -> Errors.raise_internal_error (Printf.sprintf "Redeclaration of mvar ''%s" (string_of_mvar v))

let extend_mvarnl (v: mvar) (v': mvar) (t: ftype) (ctx: context) : context =
    match find_mvar v' ctx with
    | None -> (MVarNl (v, v', t, ref 0)) :: ctx
    | _ -> Errors.raise_internal_error (Printf.sprintf "Redeclaration of mvar '%s'" (string_of_mvar v))

let extend_mvars vs ts ctx =
    List.fold_right2 extend_mvar vs ts ctx

let replace_mvar_with_mvars (x: mvar) (xs: mvar list) (ts: ftype list) (ctx: context) =
    let xs' m = List.map2 (fun x t -> MVar (x, t, m)) xs ts in
    let xs'' y' m = List.flatten (List.map2 (fun x t -> [MVarNl (x, y', t, m)]) xs ts) in
    let rec aux = function
    | [] -> []
    | (MVarNl (y, y', t, m)) :: ys' when mvar_eq x y -> (xs'' y' m) @ ys'
    | (MVar   (y, t, m))     :: ys' when mvar_eq x y -> (xs' m) @ ys'
    | y :: ys' -> y :: aux ys'
    in aux ctx

let replace_mvar_with_newly_marked_mvars (x: mvar) (xs: mvar list) (ts: ftype list) (ctx: context) =
    let xs' m = List.map2 (fun x t -> MVar (x, t, ref 0)) xs ts in
    let xs'' y' t m = List.flatten (List.map2 (fun x t -> [MVarNl (x, y', t, ref 0)]) xs ts) in
    let rec aux = function
    | [] -> []
    | (MVarNl (y, y', t, m)) :: ys' when mvar_eq x y -> (xs'' y' t m) @ ys'
    | (MVar   (y, t, m))     :: ys' when mvar_eq x y -> (xs' m) @ ys'
    | y :: ys' -> y :: aux ys'
    in aux ctx

let exchange_mvar_with_vvar (x: mvar) (mt'': ftype) (ctx: context) =
    let rec aux = function
    | [] -> assert false
    | (VVar (vv, vt)) :: (MVarNl (mv, mv', mt, mm)) :: ctx' when mvar_eq x mv -> (MVarNl (mv, mv', mt, mm)) :: (VVar (vv, vt)) :: ctx'
    | (VVar (vv, vt)) :: (MVar (mv, mt, mm))        :: ctx' when mvar_eq x mv -> (MVar (mv, mt'', mm))      :: (VVar (vv, vt)) :: ctx'
    | a :: ctx' -> a :: (aux ctx')
    in aux ctx

let find_tvar v ctx =
    let aux e = (match e with
        | TVar v' when v = v' -> true
        | _ -> false) in
    List.find_opt aux ctx

let extend_tvars (vs: tvar list) (ctx: context) : context =
    List.fold_left (fun ctx x -> 
        match find_tvar x ctx with
        | None -> (TVar x) :: ctx
        | _ -> Errors.raise_context_error (Printf.sprintf "Redeclaration of tvar '%s'" (string_of_tvar x)))
    ctx vs

let mtypeof (v: mvar) (ctx: context) : ftype =
    match find_mvar v ctx with
    | Some (MVar (_, t, _)) -> t
    | Some (MVarNl (_, _, t, _)) -> t
    | Some _ -> assert false
    | _ -> Errors.raise_context_error (Printf.sprintf "Unknown mvar '%s'" (string_of_mvar v))

let mark_mvar (v: mvar) (ctx: context) : unit =
    match find_mvar v ctx with
    | Some (MVar (_, _, m))
    | Some (MVarNl (_, _, _, m)) ->
        if !m <> 0 then
            Errors.raise_internal_error (Printf.sprintf "Multiple uses of mvar '%s'" (string_of_mvar v))
        else
            m := 1
    | _ -> assert false

let is_mvar_marked (v: mvar) (ctx: context) : bool =
    match find_mvar v ctx with
    | Some (MVar (_, _, m))
    | Some (MVarNl (_, _, _, m)) -> !m <> 0
    | _ -> assert false

let mnltypeof (v: mvar) (ctx: context) : ftype =
    match find_mvarnl v ctx with
    | Some (MVarNl (_, _, t, _)) -> t
    | Some _ -> assert false
    | _ -> Errors.raise_context_error (Printf.sprintf "Unknown mvar '%s'" (string_of_mvar v))

let vvars_after_mvar (v: mvar) (ctx: context) : fbtype list =
    let rec aux = function
        | [] -> []
        | (MVar (v', t, _))::es when mvar_eq v' v -> []
        | (MVarNl (v', _, t, _))::es when mvar_eq v' v -> []
        | (VVar (v', t))::es -> t :: aux es
        | e::es -> aux es
    in List.rev (aux ctx)

let rec until_mvar (v: mvar) = function
    | [] -> []
    | (MVarNl (v', _, t, _))::es when mvar_eq v' v -> es
    | (MVar (v', t, _))::es when mvar_eq v' v -> es
    | e::es -> until_mvar v es

let rec is_last_entry (v: mvar) = function
    | (MVarNl (v', _, t, _) :: es) when mvar_eq v v' -> true
    | (MVar (v', t, _)::es) when mvar_eq v v' -> true
    | _ -> false

let typeof_last_entry_vvar = function
    | VVar (v', t) -> t
    | _ -> assert false

let drop_last_entry = function
    | [] -> assert false
    | _ :: ctx' -> ctx'

let find_vvar v ctx =
    let aux e = ( match e with
        | VVar (v', _) when v = v' -> true
        | _ -> false) in
    List.find_opt aux ctx

let vtypeof (v: vvar) (ctx: context) : fbtype =
    match find_vvar v ctx with
    | Some (VVar (_, t)) -> t
    | _ -> Errors.raise_context_error (Printf.sprintf "Unknown vvar '%s'" (string_of_vvar v))

let extend_vvar (v: vvar) (t: fbtype) (ctx: context) : context =
    match find_vvar v ctx with
    | None -> (VVar (v, t)) :: ctx
    | _ -> Errors.raise_internal_error (Printf.sprintf "Redeclaration of vvar '%s'" (string_of_vvar v))

let extend_vvars (vs: vvar list) (ts: fbtype list) (ctx: context) : context =
    List.fold_left2 (fun c v t -> extend_vvar v t c) ctx vs ts

let unextend_mvars (vs: mvar list) (ctx: context) : context =
    let aux = function
        | MVar (v, t, m) -> let _, t' = unpack_extended t in MVar (v, t', m)
        | MVarNl (v, v', t, m) -> let _, t' = unpack_extended t in MVarNl (v, v', t', m)
        | x -> x
    in List.map aux ctx

let is_exists (t: ftype) =
    match t.ftype_desc with
    | Exists _ -> true
    | _ -> false

let rec count_vvars = function
    | [] -> 0
    | (VVar _) :: ctx' -> 1 + count_vvars ctx'
    | _ :: ctx' -> count_vvars ctx'

let vvars ctx =
    let rec aux = function
    | [] -> []
    | (VVar (v, ty)) :: ctx' -> (v, ty) :: (aux ctx')
    | _ :: ctx' -> aux ctx'
    in List.rev (aux ctx)
