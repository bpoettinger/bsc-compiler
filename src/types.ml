
type extint =
| Value of int
| Infty

let extint_compare (n1: extint) (n2: extint) : int =
    match n1, n2 with
    | Value n1', Value n2' -> n1' - n2'
    | Value n1', Infty -> -1
    | Infty, Value n2' -> 1
    | Infty, Infty -> 0

let extint_add (n1: extint) (n2: extint) : extint =
    match n1, n2 with
    | Value n1', Value n2' -> Value (n1' + n2')
    | _ -> Infty

let extint_max (n1: extint) (n2: extint) : extint =
    match n1, n2 with
    | Value n1', Value n2' -> if n1' >= n2' then n1 else n2
    | _ -> Infty

let string_of_extint = function
| Value i -> string_of_int i
| Infty -> "infty"


type label =
string

type vvar =
| VName of label
| VId of int

type tvar =
| TId of int

type mvar = {
    mvar_desc: mvar_desc;
    mutable instances: (path * mvar) list;
    mutable unique_desc: mvar_desc;
}

and mvar_desc =
| MName of label
| MId of int
| MEmpty

and path = {
    path_mvar: mvar;
    path_labels: label list;
    mutable path_unique_mvar: mvar
}

type op =
| Add
| Sub
| Mul
| Div
| Mod

let mkMVar v = { mvar_desc = v; instances = []; unique_desc = MEmpty }
let mkMName s = mkMVar (MName s)
let mkMId i = mkMVar (MId i)
let mkMEmpty () = mkMVar MEmpty

let mkPath v ls = { path_mvar = v; path_labels = ls; path_unique_mvar = mkMEmpty () }
and unpack_path p = p.path_mvar, p.path_labels
and path_mvar p = p.path_mvar
and path_labels p = p.path_labels
and path_unique_mvar p = p.path_unique_mvar

let mvar_eq v1 v2 =
    v1.mvar_desc = v2.mvar_desc

let instantiate_path p var_decl unique =
    assert (mvar_eq p.path_mvar var_decl);
    var_decl.instances <- (p, unique) :: var_decl.instances;
    p.path_unique_mvar <- unique 

let string_of_label l = l

let string_of_mvar v =
    match v.mvar_desc with
    | MName l -> l ^ "'"
    | MId i -> Printf.sprintf "m%d" i
    | MEmpty -> "MEmpty"

let string_of_vvar = function
| VName l -> l ^ "'"
| VId i -> Printf.sprintf "v%d" i

let string_of_tvar = function
| TId i -> Printf.sprintf "t%d" i

let string_of_path p =
    match p.path_labels with
    | [] -> Printf.sprintf "%s(%s)" (string_of_mvar p.path_mvar) (string_of_mvar p.path_unique_mvar)
    | ls -> Printf.sprintf "%s.%s(%s)" (string_of_mvar p.path_mvar) (String.concat "." ls) (string_of_mvar p.path_unique_mvar)

let string_of_op = function
| Add -> "+"
| Sub -> "-"
| Mul -> "*"
| Div -> "/"
| Mod -> "%"



