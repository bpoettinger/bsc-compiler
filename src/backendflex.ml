open Types
open Backend
open Printer
open Ftypes
open Context


module Make (Ident: Ident.IDENT) = struct

    let counter = Ident.make_counter ()

    let make_vvar () = VId (counter ())
    let make_mvar () = mkMId (counter ())
    let make_tvar () = TId (counter ())

    type fvalue = {
        fvalue_desc: fvalue_desc;
        fvalue_ty: fbtype
    }

    and fvalue_desc =
    | Var of vvar 
    | Int of int
    | Unit
    | Tuple of fvalue list
    | Sum of int * fvalue
    | Op of fvalue * op * fvalue

    let rec pp_fvalue v p =
        match v.fvalue_desc with
        | Var v -> dumpl (string_of_vvar v) "fvalue.Var" p
        | Int x -> dumpl (string_of_int x) "fvalue.Int" p
        | Unit -> dumpl "()" "fvalue.Unit" p
        | Tuple vs -> p |> enter "fvalue.Tuple" |> dumpvs pp_fvalue vs |> leave
        | Sum (i, v) -> p |> enter "fvalue.Sum" |> dumps (string_of_int i) |> dumpv pp_fvalue v |> leave
        | Op (v1, op, v2) -> p |> enter "fvalue.Op" |> dumps (string_of_op op) |> dumpv pp_fvalue v1 |> dumpv pp_fvalue v2 |> leave

    let mkfval (desc: fvalue_desc) (ty: fbtype) = { fvalue_desc = desc; fvalue_ty = ty }

    let mkValVar v ty = mkfval (Var v) ty
    let mkValInt v = mkfval (Int v) (mkTyInt ()) 
    let mkValUnit () = mkfval Unit (mkTyUnit ())
    let mkValTuple vs = mkfval (Tuple vs) (mkTyTuple (List.map (fun v -> v.fvalue_ty) vs))
    let mkValSum i v tys = mkfval (Sum (i, v)) (mkTySum tys)
    let mkValOp v1 op v2 ty = mkfval (Op (v1, op, v2)) ty

    type value = fvalue
    let typeof_value v = v.fvalue_ty

    type fdef = {
        name: label;
        term: t
    }

    and fterm =
    | Var of mvar
    | Return of fvalue
    | LetExpr of vvar * t * t
    | LetOp of vvar * fvalue * t
    | LetTuple of vvar list * fvalue * t
    | Match of fvalue * (int * vvar * t) list
    | Record of (label * t) list
    | LetRecord of (label * mvar) list * t * t
    | TAbs of tvar list * t
    | TApp of t * (fbtype list)
    | FbAbs of vvar * fbtype * t
    | FbApp of t * fvalue
    | FAbs of mvar * ftype * t
    | FApp of t * t
    | Pack of fbtype list * t
    | LetPack of tvar list * mvar * t * t
    | Star of fbtype
    | CloseR of t * fbtype
    | CloseL of t 
    | Derelict of mvar * t
    | Contract of t * mvar * mvar list * fbtype list
    | Dig of mvar * t * fbtype list
    | Weak of mvar * t
    | CloseRInv of t
    | Subtype of mvar * t
    | Duplicate of mvar * t

    and t = { flex_desc: fterm; flex_ty: ftype; flex_ctx: Context.context }

    let typeof t = t.flex_ty
    let ctxof t = t.flex_ctx

    let rec pp_fitem ((l, t): label * t) (p: Printer.t) : Printer.t =
        p |> enterk (string_of_label l) "fitem" |> pp_fterm t |> leave

    and pp_fterm (t: t) (p: Printer.t): Printer.t =
        match t.flex_desc with
        | Var v -> dumpl (string_of_mvar v) "fterm.Var" p
        | Return v -> p |> enter "fterm.Return" |> dumpv pp_fvalue v |> leave
        | LetExpr (v, e1, e2) -> p |> enter "fterm.LetExpr" |> dumpkv "e1" pp_fterm e1 |> dumpkv "e2" pp_fterm e2 |> leave
        | LetOp (_, v, e) -> p |> enter "fterm.LetOp" |> dumpv pp_fvalue v |> dumpv pp_fterm e |> leave
        | LetTuple (vs, v, e) -> p |> enterk (String.concat "," (List.map string_of_vvar vs)) "fterm.LetTuple" |> dumpv pp_fterm e |> leave
        | Record items -> p |> enter "fterm.Record" |> dumpvs pp_fitem items |> leave
        | Match (v, cs) -> p |> enter "fterm.Match" |> dumpv pp_fvalue v |> dumpvs (fun (i, x, v) p -> p |> dumps (string_of_int i) |> pp_fterm v) cs |> leave
        | FAbs (v, t, e) -> p |> enterk (string_of_mvar v) "fterm.FAbs" |> dumpv pp_ftype t |> dumpv pp_fterm e |> leave
        | FApp (e1, e2) -> p |> enter "fterm.FApp" |> dumpv pp_fterm e1 |> dumpv pp_fterm e2 |> leave
        | FbAbs (v, t, e) -> p |> enter "fterm.BAbs" |> dumpv pp_fbtype t |> dumpv pp_fterm e |> leave
        | FbApp (e1, e2) -> p |> enter "fterm.BApp" |> dumpv pp_fterm e1 |> dumpv pp_fvalue e2 |> leave
        | TAbs (ts, e) -> p |> enter "fterm.TAbs" |> dumps (String.concat "," (List.map string_of_tvar ts)) |> dumpv pp_fterm e |> leave
        | TApp (e1, ts) -> p |> enter "fterm.TApp" |> dumpv pp_fterm e1 |> dumpvs pp_fbtype ts |> leave
        | LetRecord (items, e1, e2) -> p |> enterk (String.concat "," (List.map (fun (l,v) -> string_of_label l ^ ";" ^ string_of_mvar v) items)) "fterm.LetRecord" |> dumpv pp_fterm e1 |> dumpv pp_fterm e2 |> leave
        | LetPack (ts, v, e1, e2) -> p |> enterk ((String.concat "," (List.map string_of_tvar ts)) ^ "; " ^ string_of_mvar v) "fterm.LetPack" |> dumpv pp_fterm e1 |> dumpv pp_fterm e2 |> leave
        | Star t -> p |> enter "fterm.Star" |> dumpv pp_fbtype t |> leave
        | CloseR (e, t) -> p |> enter "fterm.CloseR" |> dumpv pp_fbtype t |> dumpv pp_fterm e |> leave
        | CloseL e -> p |> enter "fterm.CloseL" |> dumpv pp_fterm e |> leave
        | Contract (e, x, xs, tys) -> p |> enterk (string_of_mvar x ^ ";" ^ (String.concat "," (List.map string_of_mvar xs))) "fterm.Contract" |> dumpv pp_fterm e |> leave
        | Derelict (vs, e) -> p |> enterk (string_of_mvar vs) "fterm.Derelict" |>dumpv pp_fterm e |> leave
        | Pack (ts, e) -> p |> enter "fterm.Pack" |> dumpvs pp_fbtype ts |> dumpv pp_fterm e |> leave
        | Dig (x, e, tys) -> p |> enter "fterm.Dig" |> dumpvs pp_fbtype tys |> dumps (string_of_mvar x) |> dumpv pp_fterm e |> leave
        | Weak (x, e) -> p |> enter "fterm.Weak" |> dumps (string_of_mvar x) |> dumpv pp_fterm e |> leave
        | CloseRInv e -> p |> enter "fterm.CloseRInv" |> dumpv pp_fterm e |> leave
        | Subtype (x, e) -> p |> enter "fterm.Subtype" |> dumps (string_of_mvar x) |> dumpv pp_fterm e |> leave
        | Duplicate (x, e) -> p |> enter "fterm.Duplicate" |> dumps (string_of_mvar x) |> dumpv pp_fterm e |> leave

    let mkt desc ty ctx = { flex_desc = desc; flex_ty = ty; flex_ctx = ctx }
    let mkVar v = mkt (Var v)
    let mkReturn v = mkt (Return v)
    let mkLetExpr v e1 e2 = mkt (LetExpr (v, e1, e2))
    let mkLetOp v x e = mkt (LetOp (v, x, e))
    let mkLetTuple vs x e = mkt (LetTuple (vs, x, e))
    let mkMatch x cs = mkt (Match (x, cs))
    let mkRecord items = mkt (Record items)
    let mkLetRecord items e1 e2 = mkt (LetRecord (items, e1, e2))
    let mkTabs vs e = mkt (TAbs (vs, e))
    let mkTapp e ts = mkt (TApp (e, ts))
    let mkBabs v t e = mkt (FbAbs (v, t, e))
    let mkBapp e v = mkt (FbApp (e, v))
    let mkFabs v t e = mkt (FAbs (v, t, e))
    let mkFapp e1 e2 = mkt (FApp (e1, e2))
    let mkPack ts e = mkt (Pack (ts, e))
    let mkLetPack ts x e1 e2 = mkt (LetPack (ts, x, e1, e2))
    let mkStar t = mkt (Star t)
    let mkCloseR t e = mkt (CloseR (e, t))
    let mkCloseL e = mkt (CloseL e)
    let mkDerelict x e = mkt (Derelict (x, e))
    let mkContract x xs e tys = mkt (Contract (e, x, xs, tys))
    let mkDerelict x e = mkt (Derelict (x, e))
    let mkDig x e tys = mkt (Dig (x, e, tys))
    let mkWeak x e = mkt (Weak (x, e))
    let mkCloseRInv e = mkt (CloseRInv e)
    let mkSubtype x e = mkt (Subtype (x, e))
    let mkDuplicate x e = mkt (Duplicate (x, e))


















    open Stypes



    type entry = bpath * bpath
    type ss = entry list

    let rec resolve x ss = 
        (*print_endline (String.concat "," (List.map (fun (x,y) -> string_of_bpath x ^ "->" ^ string_of_bpath y) ss));*)
        match ss with
        | [] -> x
        | (x', y') :: ss' ->
            match box_subs_opt x' x y' with
            | Some z -> (*print_endline (string_of_bpath x ^ "=>" ^ string_of_bpath z);*) z
            | None -> resolve x ss'

    let extend x y ss = 
        (*print_endline ((string_of_bpath x ^ "->" ^ string_of_bpath y) ^ ": " ^ String.concat "," (List.map (fun (x,y) -> string_of_bpath x ^ "->" ^ string_of_bpath y) ss));*)
        (x, resolve y ss) :: ss

    let subs_decl pattern substitute ss decl = 
        Interface.adjust_name (fun name -> resolve (box_subs pattern name substitute) ss) decl



    let fragments = ref []
    let add_fragment fragment = fragments := fragment :: !fragments


    let define_decl (decl: Interface.decl) (body: sexpr) =
        add_fragment (Impl.mkDef decl.name decl.args decl.ty body)

    let call_decl (decl: Interface.decl) =
        mkSCall decl.name (List.map (fun (x, ty) -> mkSValVar x ty) decl.args)

    let insert_args (n: int) (new_args: Interface.arg list) (decl: Interface.decl) =
        { decl with args = Myutil.list_insert_n n decl.args new_args }

    let match_decls (decls1: Interface.decl list) (decls2: Interface.decl list) =
        let decls1' = List.stable_sort (fun d1 d2 -> Interface.compare_name d1 d2) decls1 in
        let decls2' = List.stable_sort (fun d1 d2 -> Interface.compare_name d1 d2) decls2 in
        let rec aux d1 d2 =
            match d1, d2 with
            | [], _ -> []
            | _, [] -> []
            | x::d1', y::d2' when Interface.compare_name x y < 0 -> aux d1' d2
            | x::d1', y::d2' when Interface.compare_name x y > 0 -> aux d1 d2'
            | x::d1', y::d2' -> (x, y) :: (aux d1' d2')
        in aux decls1' decls2'

    let match_provides (iface1: Interface.t) (iface2: Interface.t) =
        match_decls iface1.provides iface2.provides

    let match_requires (iface1: Interface.t) (iface2: Interface.t) =
        match_decls iface1.requires iface2.requires
    
    let forward (iface: Interface.t) (x: bpath) (y: bpath) ss =
        let subs_x = subs_decl (mkBHole []) x ss in
        let subs_y = subs_decl (mkBHole []) y ss in
        Interface.iter_provides (fun d -> define_decl (subs_y d) (call_decl (subs_x d))) iface;
        Interface.iter_requires (fun d -> define_decl (subs_x d) (call_decl (subs_y d))) iface

    let trap (x: bpath) (iface: Interface.t) ss =
        let subs_x d = subs_decl (mkBHole []) x ss d in
        Interface.iter_requires (fun d -> define_decl (subs_x d) (call_decl (subs_x d))) iface

    let dig (iface: Interface.t) (num_vvars: int) (x: bpath) (y: bpath) (tys: stype list) ss =
        let var_x = make_vvar () in
        let ty_x = mkSTyTuple tys in
        let var_xs = List.map (fun _ -> make_vvar ()) tys in
        let arg_x = [(var_x, ty_x)] in
        let arg_xs = List.combine var_xs tys in
        let subs_x decl = insert_args num_vvars arg_x (subs_decl (mkBHole []) x ss decl) in
        let subs_xs decl = insert_args num_vvars arg_xs (subs_decl (mkBHole []) y ss decl) in
        let body_x decl = mkSLetExpr var_x (mkSReturn (mkSValTuple (List.map2 (fun v ty -> mkSValVar v ty) var_xs tys))) (call_decl (subs_x decl)) in
        let body_xs decl = mkSLetTuple var_xs (mkSValVar var_x ty_x) (call_decl (subs_xs decl)) in
        Interface.iter_provides (fun d -> define_decl (subs_xs d) (body_x d)) iface;
        Interface.iter_requires (fun d -> define_decl (subs_x d) (body_xs d)) iface

    let derelict (iface: Interface.t) (num_vvars: int) (x: bpath) (y: bpath) (value: svalue) ss =
        let var_v = make_vvar () in
        let arg_v = [(var_v, value.ty)] in
        let subs_y = subs_decl (mkBHole []) y ss in
        let subs_x decl = insert_args num_vvars arg_v (subs_decl (mkBHole []) x ss decl) in
        let body_v decl = mkSLetExpr var_v (mkSReturn value) (call_decl (subs_x decl)) in
        Interface.iter_provides (fun d -> define_decl (subs_y d) (body_v d)) iface;
        Interface.iter_requires (fun d -> define_decl (subs_x d) (call_decl (subs_y d))) iface

    let coerce (iface1: Interface.t) (iface2: Interface.t) (x: bpath) (y: bpath) ss =
        let sub_y = subs_decl (mkBHole []) y ss in
        let sub_x = subs_decl (mkBHole []) x ss in
        let generate_term1 (d1, d2) =
            let vars = List.map (fun _ -> make_vvar ()) (Interface.args d1) in
            let vvar, vvar2 = make_vvar (), make_vvar () in
            let d1' = Interface.rename_args vars d1 in
            define_decl (sub_y d2)
                (List.fold_right2 (fun (v1, t1) (v2, t2) e ->
                    mkSCoerc v1 t1 (mkSValVar v2 t2) e)
                d1'.args d2.args (mkSLetExpr vvar (call_decl (sub_x d1')) (
                        mkSUncoerc vvar2 d2.ty (mkSValVar vvar d1.ty) (mkSReturn (mkSValVar vvar2 d2.ty))))) in
        let generate_term2 (d1, d2) =
            let vars = List.map (fun _ -> make_vvar ()) (Interface.args d1) in
            let vvar, vvar2 = make_vvar (), make_vvar () in
            let d2' = Interface.rename_args vars d2 in
            define_decl (sub_x d1)
                (List.fold_right2 (fun (v1, t1) (v2, t2) e ->
                    mkSUncoerc v2 t2 (mkSValVar v1 t1) e)
                d1.args d2'.args (mkSLetExpr vvar (call_decl (sub_y d2')) (
                        mkSCoerc vvar2 d1.ty (mkSValVar vvar d2.ty) (mkSReturn (mkSValVar vvar2 d1.ty))))) in
        List.iter generate_term1 (match_provides iface1 iface2);
        List.iter generate_term2 (match_requires iface1 iface2)

    let coerceInv (iface1: Interface.t) (iface2: Interface.t) (x: bpath) (y: bpath) ss =
        let sub_y = subs_decl (mkBHole []) y ss in
        let sub_x = subs_decl (mkBHole []) x ss in
        let generate_term1 (d1, d2) =
            let vars = List.map (fun _ -> make_vvar ()) (Interface.args d1) in
            let vvar, vvar2 = make_vvar (), make_vvar () in
            let d1' = Interface.rename_args vars d1 in
            define_decl (sub_y d2)
                (List.fold_right2 (fun (v1, t1) (v2, t2) e ->
                    mkSUncoerc v1 t1 (mkSValVar v2 t2) e)
                d1'.args d2.args (mkSLetExpr vvar (call_decl (sub_x d1')) (
                        mkSCoerc vvar2 d2.ty (mkSValVar vvar d1.ty) (mkSReturn (mkSValVar vvar2 d2.ty))))) in
        let generate_term2 (d1, d2) =
            let vars = List.map (fun _ -> make_vvar ()) (Interface.args d1) in
            let vvar, vvar2 = make_vvar (), make_vvar () in
            let d2' = Interface.rename_args vars d2 in
            define_decl (sub_x d1)
                (List.fold_right2 (fun (v1, t1) (v2, t2) e ->
                    mkSCoerc v2 t2 (mkSValVar v1 t1) e)
                d1.args d2'.args (mkSLetExpr vvar (call_decl (sub_y d2')) (
                        mkSUncoerc vvar2 d1.ty (mkSValVar vvar d2.ty) (mkSReturn (mkSValVar vvar2 d1.ty))))) in
        List.iter generate_term1 (match_provides iface1 iface2);
        List.iter generate_term2 (match_requires iface1 iface2)

    let contract (iface: Interface.t) (num_vvars: int) (x: bpath) (xs: bpath list) (tys: stype list) ss =
        let var_v = make_vvar () in
        let ty_v = mkSTySum tys in
        let var_vs = List.map (fun _ -> make_vvar ()) xs in
        let arg_v = [(var_v, mkSTySum tys)] in
        let arg_vs x ty = [(x, ty)] in
        let sub_x decl = insert_args num_vvars arg_v (subs_decl (mkBHole []) x ss decl) in
        let sub_xs v ty x decl = insert_args num_vvars (arg_vs v ty) (subs_decl (mkBHole []) x ss decl) in
        let body_x i v decl ty = mkSLetExpr var_v (mkSReturn (mkSValSum i (mkSValVar v ty) tys)) (call_decl (sub_x decl)) in
        let body_xs_case decl v (x, ty) = v, call_decl (insert_args num_vvars (arg_vs v ty) (subs_decl (mkBHole []) x ss decl)) in
        let body_xs decl = mkSMatch (mkSValVar var_v ty_v) (List.map2 (body_xs_case decl) var_vs (List.combine xs tys)) in
        Interface.iter_requires (fun d -> define_decl (sub_x d) (body_xs d)) iface;
        List.iter2 (
            fun (i, x') (v', ty') -> 
                Interface.iter_provides (fun d -> define_decl (sub_xs v' ty' x' d) (body_x i v' d ty')) iface)
            (Myutil.count xs)
            (List.combine var_vs tys)





    let rec elaborate_context' ctx =
        match ctx with
        | [] -> []
        | (TVar v)::ctx' ->
            elaborate_context' ctx'
        | (MVarNl (v, _, t, _)) :: ctx'
        | (MVar (v, t, _))::ctx' ->
            let types1 = elaborate_context' ctx' in
            types1
        | (VVar (v, t))::ctx' -> 
            let types1 = elaborate_context' ctx' in
            let type2 = elaborate_fbtype t in
            (v, type2) :: types1
    
    and elaborate_context ctx =
        let types = elaborate_context' ctx in
        List.rev types

    and elaborate_fbtype (ty: Ftypes.fbtype) : stype =
        match !((find ty).fbtype_desc) with
        | Unit -> mkSTyUnit ()
        | Int -> mkSTyInt ()
        | Raw k -> mkSTyRaw !k
        | Var v -> mkSTyRef (fun () -> let t = elaborate_fbtype ty in match !((find ty).fbtype_desc) with Var _ -> mkSTyRaw Infty | _ -> t)
        | Tuple ts -> mkSTyTuple (List.map elaborate_fbtype ts)
        | Sum ts -> mkSTySum (List.map elaborate_fbtype ts)
        | Link t -> elaborate_fbtype (Ftypes.find t)
        | _ -> assert false

    and elaborate_ftype ty ctx =
        match ty.ftype_desc with
        | Monad t ->
            let tys1 = elaborate_context ctx in
            let ty2 = elaborate_fbtype t in
            Interface.mkIface [Interface.mkDecl (mkBHole ["main"]) tys1 ty2] []
        | Extended (d, t') ->
            let new_var_x = make_vvar () in
            let ctx' = ctx |> extend_vvar new_var_x d in
            elaborate_ftype t' ctx'
        | Exists (vars, t')
        | Forall (vars, t') ->
            let ctx' = ctx |> extend_tvars vars in
            elaborate_ftype t' ctx'
        | Arrow (d, t') ->
            let ctx' = ctx |> extend_vvar (make_vvar ()) d in
            let iface1 = elaborate_ftype t' ctx' in
            let iface2 = elaborate_ftype t' ctx' in
            Interface.mkIface (Interface.provides iface1) (Interface.requires iface2)
        | Lolli (t'', t') ->
            let iface1 = elaborate_ftype t'' ctx in
            let iface2 = elaborate_ftype t' ctx in
            let iface1' = Interface.subs (mkBHole []) (mkBHole ["arg"]) (mkBHole []) (mkBHole ["arg"]) iface1 in
            let iface2' = Interface.subs (mkBHole []) (mkBHole ["res"]) (mkBHole []) (mkBHole ["res"]) iface2 in
            Interface.concat [Interface.mkIface (Interface.requires iface1') (Interface.provides iface1');
                              Interface.mkIface (Interface.provides iface2') (Interface.requires iface2')]
        | Record items ->
            let ifaces = List.map (fun (l, t) -> Interface.subs (mkBHole []) (mkBHole [l]) (mkBHole []) (mkBHole [l]) (elaborate_ftype t ctx)) items in
            Interface.concat ifaces
        | Dummy _ ->
            Interface.mkIface [] []
    
    and elaborate_fvalue v =
        match v.fvalue_desc with
        | Var x -> mkSValVar x (elaborate_fbtype v.fvalue_ty)
        | Int i -> mkSValInt i
        | Unit -> mkSValUnit ()
        | Tuple xs -> mkSValTuple (List.map elaborate_fvalue xs)
        | Sum (i, x) -> mkSValSum i (elaborate_fvalue x) (List.map elaborate_fbtype (unpack_sum (find v.fvalue_ty)))
        | Op (v1, op, v2) -> mkSValOp (elaborate_fvalue v1) op (elaborate_fvalue v2) (elaborate_fbtype v.fvalue_ty)



    let rec elaborate_fterm (tm: t) (ss: ss) : unit =
        let hole = mkBHole [] in
        let ty = tm.flex_ty
        and ctx = tm.flex_ctx in
        match tm.flex_desc with
        | Var v -> 
            print_endline "(*Var*)";
            let ctx' = ctx |> replace_mvar_with_mvars v [] [] in
            let iface = elaborate_ftype ty ctx' in
            forward iface (mkBVar v []) hole ss
        | FAbs (v, t, e) -> 
            print_endline "(* FAbs *)";
            elaborate_fterm e (ss |> extend (mkBVar v []) (mkBHole ["arg"]) |> extend hole (mkBHole ["res"]))
        | FApp (e1, e2) ->
            print_endline "(* FApp *)";
            let new_mvar = make_mvar () in
            let iface = elaborate_ftype (lolli_result_type (typeof e1)) (ctxof e1) in
            elaborate_fterm e1 (ss |> extend hole (mkBVar new_mvar []));
            elaborate_fterm e2 (ss |> extend hole (mkBVar new_mvar ["arg"]));
            forward iface (mkBVar new_mvar ["res"]) hole ss
        | FbAbs (v, t, e) ->
            print_endline "(* FbAbs *)";
            let new_mvar = make_mvar () in
            let iface = elaborate_ftype (typeof e) (ctxof e) in
            let n = Context.count_vvars ctx in
            let t' = elaborate_fbtype t in
            elaborate_fterm e (ss |> extend hole (mkBVar new_mvar []));
            let target = resolve hole ss in
            Interface.iter_provides (fun d -> define_decl (subs_decl hole target ss d) (call_decl (subs_decl hole (mkBVar new_mvar []) ss d))) iface;
            Interface.iter_requires (fun d -> define_decl (insert_args n [(v, t')] (subs_decl hole (mkBVar new_mvar []) ss d)) (call_decl (subs_decl hole target ss d))) iface
        | FbApp (e1, e2) ->
            print_endline "(* FbApp *)";
            let new_mvar = make_mvar () in
            let new_vvar = make_vvar () in
            let n = Context.count_vvars ctx in
            let iface = elaborate_ftype (arrow_result_type (typeof e1)) (ctxof e1) in
            elaborate_fterm e1 (ss |> extend hole (mkBVar new_mvar []));
            let e2' = elaborate_fvalue e2 in
            let target = resolve hole ss in
            Interface.iter_provides (fun d -> define_decl (subs_decl hole target ss d) (mkSLetExpr new_vvar (mkSReturn e2') (call_decl (insert_args n [(new_vvar, e2'.ty)] (subs_decl hole (mkBVar new_mvar []) ss d))))) iface;
            Interface.iter_requires (fun d -> define_decl (subs_decl hole (mkBVar new_mvar []) ss d) (call_decl (subs_decl hole target ss d))) iface
        | Record items ->
            print_endline "(* Record *)";
            List.iter (fun (l, e) -> elaborate_fterm e (ss |> extend hole (mkBHole [l]))) items
        | LetRecord (items, e1, e2) -> 
            print_endline "(* LetRecord *)";
            let ss' = List.fold_left (fun ss' (l, x) -> ss' |> extend (mkBHole [l]) (mkBVar x [])) ss items in
            elaborate_fterm e1 ss';
            elaborate_fterm e2 ss
        | TAbs (vs, e) ->
            print_endline "(* TAbs *)";
            let _ = elaborate_ftype (typeof e) (ctxof e) in
            elaborate_fterm e ss
        | TApp (e, ts) ->
            print_endline "(* TApp *)";
            let new_mvar = make_mvar () in
            let xs, ty' = unpack_forall (typeof e) in
            let iface1 = elaborate_ftype (subs_ftype (List.combine xs ts) ty') (ctxof e) in
            let ctx' = Context.extend_tvars xs (ctxof e) in
            let iface2 = elaborate_ftype ty' ctx' in
            elaborate_fterm e (ss |> extend hole (mkBVar new_mvar []));
            coerce iface2 iface1 (mkBVar new_mvar []) hole ss
        | Pack (ts, e) ->
            print_endline "(* Pack *)";
            let new_mvar = make_mvar () in
            let xs, ty' = unpack_exists ty in
            let iface1 = elaborate_ftype (subs_ftype (List.combine xs ts) ty') (ctxof e) in
            let ctx' = Context.extend_tvars xs (ctxof e) in
            let iface2 = elaborate_ftype ty' ctx' in
            elaborate_fterm e (ss |> extend hole (mkBVar new_mvar []));
            coerceInv iface1 iface2 (mkBVar new_mvar []) hole ss
        | LetPack (ts, x, e1, e2) ->
            print_endline "(* LetPack *)";
            elaborate_fterm e2 ss;
            elaborate_fterm e1 (ss |> extend hole (mkBVar x []));
        | Star t ->
            print_endline "(* Star *)";
            ()
        | Return v ->
            print_endline "(* Return *)";
            let _ = elaborate_context ctx in
            let vvars = Context.vvars ctx in
            let vvars' = List.map (fun (v, ty) -> v, elaborate_fbtype ty) vvars in
            let v' = elaborate_fvalue v in
            add_fragment (Impl.mkDef (resolve (mkBHole ["main"]) ss) vvars' v'.ty (mkSReturn v'))
        | LetExpr (v, e1, e2) ->
            print_endline "(* LetExpr *)";
            let new_vvar = make_vvar () in
            let x1, x2 = make_mvar (), make_mvar () in
            let ty1' = elaborate_fbtype (unpack_monad (typeof e1)) in
            let iface1 = elaborate_ftype (typeof e1) ctx in
            let iface2 = elaborate_ftype (typeof e2) ctx in
            let n = Context.count_vvars (ctxof e1) in
            let dmain = (match Interface.provide_by_name (mkBHole ["main"]) iface1 with Some d -> d | _ -> assert false) in
            let sub d = subs_decl hole (mkBVar x2 []) ss (insert_args n [(new_vvar, ty1')] d) in
            elaborate_fterm e1 (ss |> extend hole (mkBVar x1 []));
            elaborate_fterm e2 (ss |> extend hole (mkBVar x2 []));
            let target = resolve hole ss in
            Interface.iter_provides (fun d -> define_decl (subs_decl hole target ss d) (mkSLetExpr new_vvar (call_decl (subs_decl hole (mkBVar x1 []) ss dmain)) (call_decl (sub d)))) iface2;
            Interface.iter_requires (fun d -> define_decl (sub d) (call_decl (subs_decl hole target ss d))) iface2
        | LetTuple (vs, v, e) ->
            print_endline "(* LetTuple *)";
            let x = make_mvar () in
            let iface = elaborate_ftype (typeof e) ctx in
            let n = Context.count_vvars ctx in
            let v' = elaborate_fvalue v in
            let vs' = List.combine vs (unpack_stuple v'.ty) in
            elaborate_fterm e (ss |> extend hole (mkBVar x []));
            let target = resolve hole ss in
            Interface.iter_provides (fun d -> define_decl (subs_decl hole target ss d) (mkSLetTuple vs v' (call_decl (insert_args n vs' (Interface.subs_decl hole (mkBVar x []) d))))) iface
        | Match (v, cs) ->
            print_endline "(* Match *)";
            let new_vvar = make_vvar () in
            let xs = List.map (fun _ -> make_mvar ()) cs in
            let iface = elaborate_ftype ty ctx in
            let n = Context.count_vvars ctx in
            let v'' = elaborate_fvalue v in
            let target = resolve hole ss in
            let cs' decl = List.map2 (fun (i, v', e) x -> v', call_decl (insert_args n [(v', v''.ty)] (subs_decl hole (mkBVar x []) ss decl))) cs xs in
            let req' x = Interface.iter_requires (fun d -> define_decl (insert_args n [(new_vvar, v''.ty)] (subs_decl hole (mkBVar x []) ss d)) (call_decl (subs_decl hole target ss d))) iface in
            Interface.iter_provides (fun d -> define_decl (subs_decl hole target ss d) (mkSMatch v'' (cs' d))) iface;
            List.iter2 (fun (i, v, e) x -> elaborate_fterm e (ss |> extend hole (mkBVar x []))) cs xs;
            List.iter req' xs
        | LetOp (x, v, e) ->
            print_endline "(* LetOp *)";
            let new_mvar = make_mvar () in
            let iface = elaborate_ftype (typeof e) ctx in
            let n = Context.count_vvars ctx in
            elaborate_fterm e (ss |> extend hole (mkBVar new_mvar []));
            let v' = elaborate_fvalue v in
            let target = resolve hole ss in
            Interface.iter_provides (fun d -> define_decl (subs_decl hole target ss d) (mkSLetOp x v' (call_decl (insert_args n [(x, mkSTyInt ())] (Interface.subs_decl hole (mkBVar new_mvar []) d))))) iface
        | CloseR (e, t) ->
            print_endline "(* CloseR *)";
            elaborate_fterm e ss
        | CloseL e ->
            print_endline "(*CloseL*)";
            elaborate_fterm e ss
        | Contract (e, x, xs, tys) ->
            print_endline "(* Contract *)";
            let _, ty' = unpack_extended (mtypeof x ctx) in
            let ctx' = ctx |> Context.until_mvar x in
            let iface = elaborate_ftype ty' ctx' in
            let n = Context.count_vvars ctx in
            let tys' = List.map elaborate_fbtype tys in
            elaborate_fterm e ss;
            contract iface n (resolve (mkBVar x []) ss) (List.map (fun x -> resolve (mkBVar x []) ss) xs) tys' ss
        | Derelict (old_mvar, e) ->
            print_endline "(* Derelict *)";
            let new_mvar = make_mvar () in
            let ty9 = mtypeof old_mvar ctx in
            let _, ty' = unpack_extended ty9 in
            let ctx' = ctx |> Context.until_mvar old_mvar in
            let iface = elaborate_ftype ty' ctx' in
            let n = Context.count_vvars ctx' in
            elaborate_fterm e (ss |> extend (mkBVar old_mvar []) (mkBVar new_mvar []));
            derelict iface n (mkBVar old_mvar []) (mkBVar new_mvar []) (mkSValUnit ()) ss
        | Dig (x, e, tys) ->
            print_endline "(* Dig *)";
            let new_mvar = make_mvar () in
            let ty' = mtypeof x ctx in
            let ty'' = extended_inner ty' in
            let ctx' = ctx |> Context.until_mvar x in
            let iface = elaborate_ftype ty'' ctx' in
            let n = Context.count_vvars ctx' in
            let tys' = List.map elaborate_fbtype tys in
            elaborate_fterm e (ss |> extend (mkBVar x []) (mkBVar new_mvar []));
            dig iface n (mkBVar x []) (mkBVar new_mvar []) tys' ss
        | Weak (x, e) ->
            print_endline "(* Weak *)";
            let ty' = mtypeof x ctx in
            let iface = elaborate_ftype ty' ctx in
            elaborate_fterm e ss;
            trap (mkBVar x []) iface ss
        | CloseRInv e ->
            print_endline "(* CloseRInv *)";
            elaborate_fterm e ss
        | Subtype (x, e) ->
            print_endline "(* Subtype *)";
            let new_mvar = make_mvar () in
            let ctx' = ctx |> until_mvar x in
            let iface1 = elaborate_ftype (mtypeof x ctx) ctx' in
            let iface2 = elaborate_ftype (mtypeof x (ctxof e)) ctx' in
            elaborate_fterm e (ss |> extend (mkBVar x []) (mkBVar new_mvar []));
            coerce iface1 iface2 (mkBVar x []) (mkBVar new_mvar []) ss
        | Duplicate (x, e) ->
            print_endline "(* Duplicate *)";
            let new_mvar = make_mvar () in
            let new_vvar = make_vvar () in
            let ctx' = (ctxof e) |> until_mvar x in
            let ty9 = mtypeof x (ctxof e) in
            let ext', ty' = unpack_extended ty9 in
            let ext'' = elaborate_fbtype ext' in
            let iface = elaborate_ftype ty' ctx' in
            assert (Interface.is_requires_empty iface);
            let n = count_vvars ctx' in
            elaborate_fterm e (ss |> extend (mkBVar x []) (mkBVar new_mvar []));
            Interface.iter_provides (fun d -> define_decl (subs_decl hole (mkBVar new_mvar []) ss (insert_args n [(new_vvar, ext'')] d)) (call_decl (subs_decl hole (mkBVar x []) ss d))) iface

end
