

struct
    f(F) = fn (x:int) -> let y = x * 2 in return y,
    main(MAIN) = fn(x:int) -> let z = x + 3 in let y = F z in return y
end