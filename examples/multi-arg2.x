
struct
    f(F) = fn(x:int)(y:int) -> let z = x + y in return z,
    main(MAIN) = F 2 
end