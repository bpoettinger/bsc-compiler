
struct
    f(F) = functor(X:int->monad<int>)(Y:unit->monad<int>) -> struct
        ff(FF) = fn(x:int) -> let y = X x in let z = Y () in let zz = z + y in return zz 
    end,
    f1(F1) = fn(x:int) -> let y = x+1 in return y,
    f2(F2) = fn(y:unit) -> return 32,
    m(M) = F(F1)(F2),
    main(MAIN) = M.ff
end