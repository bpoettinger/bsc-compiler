
struct
    main(MAIN) = fn(w:int) -> case inl(22) as int+unit of inl(x) => let y = x * 2 in return y; inr(x) => let y = x * 3 in return y
end