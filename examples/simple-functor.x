
struct
    f(F) = functor (X: sig end) -> struct 
        ff(FF) = fn (x:int) -> let y = x + 2 in return y
    end,
    n(N) = struct end,
    m(M) = F(N),
    main(MAIN) = fn(x: int) -> M.ff 43
end