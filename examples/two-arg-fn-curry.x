
struct 
    f(F) = fn (x: int) -> fn (y: int) -> let z = x + y in return z,
    main(MAIN) = fn(x: int) -> F 2 3
end