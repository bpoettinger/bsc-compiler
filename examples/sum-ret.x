
struct
    ff(FF) = fn(x: int+int) -> case x of inl(xx) => return inr(xx) as int+int; inr(xx) => return inl(xx) as int+int,
    f(F) = fn(x: int+int) -> case x of inl(xx) => let y = xx * 2 in return y; inr(xx) => let y = xx * 3 in return y,
    main(MAIN) = fn(w:int) -> let x = return inl(22) as int+int in let xx = FF x in F xx
end