
struct
    f(F) = functor(FF: int -> monad<int>) ->
        fn(x:int) -> let x1 = FF x in let x2 = FF x1 in return x2,
    farg(FARG) = fn(x:int) -> let y = x + x in return y,
    fun(FUN) = F(FARG),
    main(MAIN) = FUN 
end