
struct
    f(F) = fn(x: int+int) -> case x of inl(xx) => let y = xx * 2 in return y; inr(xx) => let y = xx * 3 in return y,
    main(MAIN) = fn(w:int) -> let x = return inl(22) as int+int in F x
end