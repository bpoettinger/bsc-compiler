
struct
    f(F) = fn(z:int*int) -> let (x1, x2) = z in let y = x1 + x2 in return y,
    main(MAIN) = fn(x: int) -> let zz = return (1, 2) in let y = F zz in return y 
end