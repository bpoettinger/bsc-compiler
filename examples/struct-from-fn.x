
struct 
    f(F) = fn(x: int) -> struct ff(FF) = fn(z: int) -> let y = x * z in return y end,
    m(M) = F 2,
    main(MAIN) = fn (x: int) -> M.ff 7
end