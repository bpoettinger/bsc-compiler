struct
    f(F) = functor (X: sig ff(FF): int -> int -> monad<int> end) -> struct
        fff(FFF) = fn (x:int) -> X.ff x x
    end,
    d(D) = struct
        ff(FF) = fn(x:int)(y:int) -> let z = x * y + 2 in return z + 1
    end,
    m(M) = F(D),
    main(MAIN) = fn(x: int) -> M.fff (43 % 23)
end
