
struct
    f(F) = fn(x: int) -> return x,
    main(MAIN) = fn(x: int) -> F ()
end