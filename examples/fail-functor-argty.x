
struct
    f(F) = functor(FF: unit -> monad<int>) -> struct fff(FFF) = fn(x:int) -> let y = FF () in return y end,
    ffff(FFFF) = fn(x:int) -> return 2,
    m(M) = F(FFFF),
    main(MAIN) = M.fff
end