
struct
    m(M) = struct t(T) = type int end,
    f(F) = fn(x:M.t) -> return x,
    main(MAIN) = fn(x:int) -> F x
end