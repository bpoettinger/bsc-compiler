
struct
    f(F) = functor(X: int -> monad<int>) -> fn(x:int) -> let y = X x in let z = X y in return z,
    g(G) = fn(x:int) -> return 2 * x,
    main(MAIN) = F(G)
end