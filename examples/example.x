
struct

    a(A) = struct
        m(M) = type int,
        n(N) = fn(x:int) -> let y = x + x in return y,
        k(K) = fn(x:int) -> let y = N x in return y,
        l(L) = fn(x:int) -> let y = K x in let yy = K y in return yy
    end,

    double(DOUBLE) = functor (X:sig
        m(M): type,
        l(L): M -> monad<M>
    end) -> struct
        m(M) = type X.m,
        d(D) = fn (x: M) -> let y = X.l x in let yy = X.l y in return yy
    end,

    double_t(DOUBLE_T) = DOUBLE(A),

    main(MAIN) = fn(x:int) -> DOUBLE_T.d x

end
