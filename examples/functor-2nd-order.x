
struct
    f(F) = functor(FF: functor(FFF: int -> monad<int>) -> sig e(E): int -> monad<int> end) -> struct
            a1(A1) = fn(x:int) -> let y = x * 2 in return y,
            a2(A2) = fn(x:int) -> let y = x * 3 in return y,
            b1(B1) = FF(A1),
            b2(B2) = FF(A2),
            c(C) = fn(z:int) -> let d1 = B1.e z in let d2 = B2.e z in let d3 = d1 + d2 in return d3
        end,
    f2(F2) = functor(FFF: int -> monad<int>) -> struct
            e(E) = fn(u:int) -> let u1 = FFF u in let u2 = FFF u1 in return u2
        end,
    m(M) = F(F2),
    main(MAIN) = fn(w:int) -> let w1 = M.c w in return w1
end