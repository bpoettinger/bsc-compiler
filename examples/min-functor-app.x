
struct
    f(F) = functor (X: sig end) -> struct end,
    a(A) = struct end,
    m(M) = F(A),
    main(MAIN) = fn(x:int) -> return 32
end
