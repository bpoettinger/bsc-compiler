
struct
    f(F) = functor(X: int -> monad<int>) -> struct
            g(G) = X
        end,
    h(H) = fn(x: int) -> let y = 3 * x in return y,
    m(M) = F(H),
    mm(MM) = M,
    main(MAIN) = MM.g
end
