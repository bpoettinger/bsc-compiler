
struct
    f(F) = functor (X: sig ff(FF): int -> monad<int> end) -> struct
        fff(FFF) = fn (x:int) -> X.ff x
    end,
    d(D) = struct
        ff(FF) = fn(x:int) -> let y = x * 2 in return y
    end,
    m(M) = F(D),
    main(MAIN) = fn(x: int) -> M.fff 43
end