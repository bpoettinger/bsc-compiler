
struct
    f(F) = functor(F: int -> monad<int>) -> struct g(G) = functor(F : int -> monad<int>) -> struct end end,
    main(MAIN) = fn(x:int) -> return x
end