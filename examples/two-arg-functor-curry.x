
struct
    f(F) = functor(X: sig f1(F1): int -> monad<int> end) ->
        functor(Y: sig f2(F2): int -> monad<int> end) ->
            struct ff(FF) = fn(w: int) -> let w1 = X.f1 w in let w2 = Y.f2 w1 in return w2 end,
    xx(XX) = struct f1(F1) = fn(y:int) -> let yy = y + 2 in return yy end,
    yy(YY) = struct f2(F2) = fn(z:int) -> let zz = z + 3 in return zz end,
    m(M) = F(XX)(YY),
    main(MAIN) = fn(x: int) -> M.ff x
end