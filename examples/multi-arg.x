
struct
    f(F) = fn(x:int)(y:int) -> let z = x + y in return z,
    main(MAIN) = fn(x:int) -> let y = F x 1 in return y
end