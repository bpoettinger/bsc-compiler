

struct
    f(F) = functor(FF: functor(FFF: int -> monad<int>) -> sig e(E): int -> monad<int> end) -> struct
            c(C) = fn(z:int) -> let zz = z + 3 in return zz
        end,
    f2(F2) = functor(FFF: int -> monad<int>) -> struct
            e(E) = fn(u:int) -> let u1 = FFF u in let u2 = FFF u1 in return u2
        end,
    m(M) = F(F2),
    main(MAIN) = fn(w:int) -> let w1 = M.c w in return w1
end
