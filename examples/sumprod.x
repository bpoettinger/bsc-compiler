
struct
    t1(T1) = type (int*int)+int,
    t2(T2) = type unit+(int*((int*int)+int)),
    t3(T3) = type (unit+(int*((int*int)+int))) + unit,
    mk(MK) = fn(x: int) -> return inl(inr((10, inl ((11,12)) as (int*int)+int)) as unit+(int*((int*int)+int))) as (unit+(int*((int*int)+int))) + unit,
    calc3(CALC3) = fn(c:T1) -> case c of inl(xx) => let (xx1,xx2) = xx in let yy = xx1+xx2 in return yy; inr(xx) => return 97,
    calc4(CALC4) = fn(d:int*T1) -> let (x, y) = d in let z = CALC3 y in let zz = x+z in return zz,
    calc2(CALC2) = fn(b:T2) -> case b of inl(xx) => return 98; inr(xx) => CALC4 xx,
    calc(CALC) = fn(a:T3) -> case a of inl(xx) => CALC2 xx; inr(aa) => return 99,
    main(MAIN) = fn(w:int) -> let v = MK w in CALC v
end