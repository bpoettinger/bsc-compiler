
struct
    f1(F1) = fn(x:int) -> let y = x + 1 in return y,
    f2(F2) = F1,
    main(MAIN) = F2
end