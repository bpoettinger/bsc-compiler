
struct
    f(F) = functor (X: sig fff(FFFF): int -> monad<int> end) -> struct ff(FF) = fn (x:int) -> let y = X.fff x in return y end,
    a(A) = struct fff(FFF) = fn(x: int) -> return x end,
    m(M) = F(A),
    main(MAIN) = fn(x:int) -> let y = M.ff x in return y
end
