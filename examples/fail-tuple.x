
struct
    main(MAIN) = fn(x:int) -> let y = return (x, ()) in let (y1, y2) = y in let z = y1 + y2 in return z
end
