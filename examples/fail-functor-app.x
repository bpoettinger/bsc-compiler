
struct
    f(F) = functor(X: sig fff(FFF): int -> monad<int> end) -> struct ff(FF) = fn(x: int) -> return x end,
    mm(MM) = struct fff(FFF) = fn(x: unit) -> return 3 end,
    m(M) = F(MM),
    main(MAIN) = M.ff
end