
struct 
    f(F) = fn (x: int) -> fn (y: int) -> let z = x + y in return z,
    ff(FF) = F 2,
    main(MAIN) = fn(x: int) -> FF 3
end