

struct
    m(M) = struct
        f(F) = fn (x:int) -> let y = x * 2 in return y
    end,
    main(MAIN) = fn(x: int) -> let z = return 3 in let y = M.f z in return y
end