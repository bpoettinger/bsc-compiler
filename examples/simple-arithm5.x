

struct
    m(M) = struct
        t(T) = type int,
        f(F) = fn (x: T) -> let y = x * 2 in return y
    end,
    f2(F2) = fn(x: M.t) -> let y = x - 1 in return y,
    main(MAIN) = fn(x: M.t) -> let z = return 3 in let zz = z + 4 in let zzz = F2 zz in let y = M.f zzz in return y
end