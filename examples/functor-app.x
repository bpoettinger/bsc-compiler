
struct 
    s(S) = struct 
        ff(FF) =
            functor (X:sig t(T): type=int, f(F): T->monad<T> end) -> struct 
                t(T) = type X.t, 
                f(F) = fn (y:X.t) -> let x1 = X.f 44 in let x2 = x1+y in return x2
            end,
        t(T) = type int, 
        f(F) = fn(x:T) -> let x1 = x+1 in return x1 
    end,
    mf(MF) = fn(x: int) -> S.ff(S),
    m(M) = MF 2,
    main(MAIN) = fn(x:int) -> M.f 6 
end
