
struct
    ff(FF) = fn(a:int*int) -> let (a1,a2) = a in return (a2, a1),
    f(F) = fn(z:int*int) -> let (x1, x2) = z in let xx1 = x1 * 9 in let y = xx1 + x2 in return y,
    main(MAIN) = fn(x: int) -> let zz = return (1, 2) in let yy = FF zz in let y = F yy in return y 
end