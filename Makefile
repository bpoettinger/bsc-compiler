

FLAGS = -use-ocamlfind -I src

CC = ocamlbuild

all: build_main test

clean:
	$(CC) -clean

build_main:
	$(CC) $(FLAGS) main.byte

test: build_main
	./test/test.py
