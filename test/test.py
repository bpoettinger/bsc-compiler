#! /usr/bin/env python

import unittest
import subprocess
import tempfile
from pathlib import Path


class Test(unittest.TestCase):

    def assert_result(self, testfile, expected):
        path = Path('.') / 'examples' / testfile
        destfile = tempfile.NamedTemporaryFile(suffix='.ml', delete=False)
        exepath = Path(destfile.name).with_suffix('.exe')
        ret = subprocess.call(['./main.byte', path], stdout=destfile, stderr=subprocess.STDOUT)
        if ret != 0:
            self.fail(f"Test '{path}' failed: elaborate")
        destfile.flush()
        ret = subprocess.call(['ocamlopt', '-o', exepath, destfile.name])
        if ret != 0:
            self.fail(f"Test '{path}' failed: compile'")
        actual = subprocess.check_output([exepath])
        self.assertEqual(expected + b'\n', actual)

    def test_simple_arithm(self): self.assert_result('simple-arithm.x', b'8')
    def test_simple_arithm2(self): self.assert_result('simple-arithm2.x', b'6')
    def test_simple_arithm3(self): self.assert_result('simple-arithm3.x', b'8')
    def test_simple_arithm4(self): self.assert_result('simple-arithm4.x', b'10')
    def test_simple_arithm5(self): self.assert_result('simple-arithm5.x', b'12')
    def test_simple_arithm6(self): self.assert_result('simple-arithm6.x', b'2')
    def test_simple_functor(self): self.assert_result('simple-functor.x', b'45')
    def test_struct_from_fn(self): self.assert_result('struct-from-fn.x', b'14')
    def test_typevar(self): self.assert_result('typevar.x', b'1')
    def test_functor(self): self.assert_result('functor.x', b'86')
    def test_min_functor(self): self.assert_result('min-functor-app.x', b'32')
    def test_mid_functor(self): self.assert_result('mid-functor-app.x', b'1')
    def test_example(self): self.assert_result('example.x', b'16')
    def test_functor_app(self): self.assert_result('functor-app.x', b'51')
    def test_two_arg_fn(self): self.assert_result('two-arg-fn.x', b'5')
    def test_two_arg_fn_curry(self): self.assert_result('two-arg-fn-curry.x', b'5')
    def test_two_arg_functor_curry(self): self.assert_result('two-arg-functor-curry.x', b'6')
    def test_assign_mvar(self): self.assert_result('assign-mvar.x', b'2')
    def test_fn_as_functor_arg(self): self.assert_result('fn-as-functor-arg.x', b'4')
    def test_chain_functor_with_fn_call(self): self.assert_result('chain-functor-with-fn-call.x', b'4')
    def test_functor_2nd_order(self): self.assert_result('functor-2nd-order.x', b'13')
    def test_functor_3rd_order(self): self.assert_result('functor-3rd-order.x', b'48')
    def test_member_of_functor(self): self.assert_result('member-of-functor.x', b'3')
    def test_sum(self): self.assert_result('sum.x', b'44')
    def test_sum_arg(self): self.assert_result('sum-arg.x', b'44')
    def test_sum_ret(self): self.assert_result('sum-ret.x', b'66')
    def test_product(self): self.assert_result('product.x', b'3')
    def test_product_arg(self): self.assert_result('product-arg.x', b'3')
    def test_unused_functor_arg(self): self.assert_result('unused-functor-arg.x', b'4')
    def test_unused_member(self): self.assert_result('unused-member.x', b'1')
    def test_arithmetic(self): self.assert_result('arithmetic.x', b'22')
    def test_arithmetic2(self): self.assert_result('arithmetic2.x', b'22')
    def test_arithmetic3(self): self.assert_result('arithmetic3.x', b'11')
    def test_arithmetic4(self): self.assert_result('arithmetic4.x', b'7')
    def test_arithmetic5(self): self.assert_result('arithmetic5.x', b'12')
    def test_arithmetic6(self): self.assert_result('arithmetic6.x', b'12')
    def test_product_ret(self): self.assert_result('product-ret.x', b'19')
    def test_product_and_sum(self): self.assert_result('sumprod.x', b'33')
    def test_multi_arg(self): self.assert_result('multi-arg.x', b'2')
    def test_multi_arg2(self): self.assert_result('multi-arg2.x', b'3')
    def test_multi_arg_functor(self): self.assert_result('multi-arg-functor.x', b'34')
    def test_embedded_fn(self): self.assert_result('embeded-fn.x', b'4')
    def test_embedded_functor(self): self.assert_result('embeded-functor.x', b'13')
    def test_multi_sum(self): self.assert_result('multi-sum.x', b'6')
    def test_multi_prod(self): self.assert_result('multi-prod.x', b'11')
    def test_multi_arg_functor(self): self.assert_result('multi-arg-functor.x', b'34')
    def test_monad(self): self.assert_result('monad.x', b'43')
    def test_multi_arrow(self): self.assert_result('multi-arrow.x', b'4')
    def test_blub(self): self.assert_result('blub.x', b'403')

if __name__ == '__main__':
    unittest.main()
